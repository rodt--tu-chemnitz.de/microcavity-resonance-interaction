import numpy as np
import os
import pandas as pd

import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

import sys
sys.path.append('../..')
import python.plotparams


def main():
    print('PLOTTING HARMINV RESULTS')


    symmetric_dir = 'results/symmetric'
    asymmetric_dir = 'results/antisymmetric'
    csvdir_list = [symmetric_dir, asymmetric_dir]

    
    # relevant frequencies
    f_min = 0.61
    f_max = 0.63

    fig, ax = plt.subplots(figsize=(6,3))

    marker_list = ['D', 'o', 'v']
    color_list = ['C{0}'.format(i) for i in range(10)]
    for csvdir, color, marker in zip(csvdir_list, color_list, marker_list):
        print(csvdir)

        distdirs = sorted(
            os.listdir(csvdir),
            key=lambda s: float(s[:-4].split('=')[1])
            )
        
        frequency_listlist = []
        Q_listlist = []
        dist_list = []
        
        for dd in distdirs:
            dist = float(dd[:-4].split('=')[1])
            dist_list.append(dist)

            filename_csv = '{0}/{1}'.format(csvdir, dd)
            data = pd.read_csv(
                filename_csv,
                sep=', ',
                engine='python'
                )
            
            frequency_list = data['frequency'].values
            Q_list = data['Q'].values

            frequency_bool_list = (f_min < frequency_list) & (frequency_list < f_max)

            frequency_listlist.append(frequency_list[frequency_bool_list])
            Q_listlist.append(Q_list[frequency_bool_list])
        

        # Q_max = np.max(np.abs(np.concatenate(Q_listlist)))

        # determine color

        for d, f_l, Q_l in zip(dist_list, frequency_listlist, Q_listlist):
            Q_max = np.max(np.abs(Q_l))

            for f, Q in zip(f_l, Q_l):
                Q_abs = np.abs(Q)
                alpha = Q_abs / Q_max
                # alpha = 1.

                ax.plot(
                    d, f,
                    alpha=alpha,
                    marker=marker,
                    color=color,
                    markeredgecolor='black'
                    )
    
    legend_elements = []
    for text, color, marker in zip(['symmetric', 'antisymmetric'], color_list, marker_list):
        l = Line2D(
            [0], [0],
            marker=marker,
            linestyle=' ',
            color=color,
            label=text,
            markeredgecolor='black'
            )
        
        legend_elements.append(l)

    ax.legend(
        handles=legend_elements,
        title='mode symmetry',
        loc='lower right'
        )

    # mark source
    fcen = 0.6206882714301404
    ax.axhline(
        fcen,
        zorder=0,
        color='black',
        linestyle='dashed'
        )
    
    ax.text(
        0.05, fcen,
        r'$f_\mathrm{src}$',
        ha='center',
        va='center',
        bbox=dict(
            boxstyle="round",
            ec='black',
            fc='white',
            )
        )

    ax.set_xlabel(r'intercavity distance $D/R$')
    ax.set_ylabel(r'frequency')

    ax.set_xlim(0, 0.5)
    ax.set_ylim(0.613, 0.625)

    # tickstep = 0.01
    # ax.set_yticks([0.615, 0.620, 0.625])

    fig.savefig('harminv_results.png')

        


if __name__ == '__main__':
    main()