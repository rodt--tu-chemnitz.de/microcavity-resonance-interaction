from matplotlib import lines
import numpy as np
import os
import pandas as pd

import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.patches import FancyArrowPatch

import sys
sys.path.append('../..')
import python.plotparams

def main():
    print('CONSTRUCTING HUSIMI REPRESENTATION')

    # load data
    # usemode = 'symmetric'
    # usemode = 'antisymmetric'

    usemode = sys.argv[1]
    useside = sys.argv[2]


    data_ez = np.loadtxt('data/mode_{0}_ez.dat'.format(usemode)).T
    data_eps = np.loadtxt('data/mode_{0}_eps.dat'.format(usemode)).T


    # obtain data

    SCALE = 50

    R = 1.
    R_scaled = R * SCALE

    DIST = 0.1
    DIST_scaled = DIST * SCALE

    plot_offset = - np.array([0.5, 0.5])
    center = np.flip(np.array(data_ez.shape)) * 0.5 + plot_offset

    # define circles / where to extract values

    n_circlepoints = 200
    dh = 2

    field_lists = []
    circle_lists = []

    circle_left_center = center - np.array([DIST_scaled * .5 + R_scaled, 0])
    circle_right_center = center + np.array([DIST_scaled * .5 + R_scaled, 0])

    for radius in [R_scaled, R_scaled-dh, R_scaled+dh]:

        circle_left_list = circle(
            center=circle_left_center,
            radius=radius,
            n_points=n_circlepoints
            )

        circle_right_list = circle(
            center=circle_right_center,
            radius=radius,
            n_points=n_circlepoints
            )

        # extract fields
        circlepoints_all = np.concatenate((circle_left_list, circle_right_list))
        field_indizes = np.int64(np.round(circlepoints_all+plot_offset))
        

        s_list = np.linspace(0, 2, 2*n_circlepoints+1)[:-1]
        field = [data_ez[i_list[1], i_list[0]] for i_list in field_indizes]

        # save arrays
        field_lists.append(field)
        circle_lists.append(circlepoints_all)
    

    field_mid, field_inner, field_outer = np.array(field_lists)
    circle_mid, circle_inner, circle_outer = np.array(circle_lists)

    if useside == 'inner':
        field_derivative = field_mid - field_inner
    elif useside == 'outer':
        field_derivative = field_outer - field_mid
    else:
        print('ERROR: useside {0} not known'.format(useside))
        raise ValueError
    field_derivative = field_derivative / dh

    print('saving border functions...')

    np.savetxt(
        'border.dat',
        [s_list, field_mid, field_derivative]
        )

    # plotting

    print('plotting...')

    # plotting system
    if False:
        print('\t- system')
        fig, ax = plt.subplots()

        for c, linestyle in zip(circle_lists, ['-', '--', '--']):
            circle_devideindex = n_circlepoints
            circle_left_list = c[:circle_devideindex]
            circle_right_list = c[circle_devideindex:]
            
            ax.plot(
                *circle_left_list.T,
                color='black',
                linestyle=linestyle
                )
            ax.plot(
                *circle_right_list.T,
                color='black',
                linestyle=linestyle
                )

        # ax.imshow(
        #     data_eps,
        #     cmap='binary',
        #     zorder=0
        #     )

        ax.imshow(
            data_ez,
            cmap='coolwarm',
            zorder=0,
            alpha=1.
            )
        
        center = np.array(data_ez.shape) * .5

        # ax.set_xlim(center[0]-25, center[0]+25)
        # ax.set_xlim(center[1]-25, center[1]+25)

        fig.savefig('system_{0}.png'.format(usemode))
        plt.close(fig)
    

    # plotting system
    if True:
        print('\t- system')
        fig, ax = plt.subplots()

        
        c = circle_mid

        circle_devideindex = n_circlepoints

        # so the circles appear closed
        circle_left_list = c[:circle_devideindex]
        circle_left_list = np.concatenate((circle_left_list, [circle_left_list[0]]))

        circle_right_list = c[circle_devideindex:]
        circle_right_list = np.concatenate((circle_right_list, [circle_right_list[0]]))
        
        ax.plot(
            *circle_left_list.T,
            color='black',
            linestyle='-'
            )
        ax.plot(
            *circle_right_list.T,
            color='black',
            linestyle='-'
            )

        # ax.imshow(
        #     data_eps,
        #     cmap='binary',
        #     zorder=0
        #     )

        ax.imshow(
            data_ez,
            cmap='coolwarm',
            zorder=0,
            alpha=1.
            )
        
        # arrows to mark s

        center = np.array(data_ez.shape) * .5 - 0.5

        # mark s=0 and s=1 at circles
        bottom_left = center + np.array([1, - 1.05]) * SCALE
        bottom_left = np.flip(bottom_left)
        bottom_right = center  + np.array([1, + 1.05]) * SCALE
        bottom_right = np.flip(bottom_right)

        for bottom in [bottom_left, bottom_right]:
            ax.plot(
                *bottom,
                marker='|',
                color='black',
                markersize=10
                )

        # add fancy arrow patches
        style = "Simple, tail_width=0.5, head_width=4, head_length=8"
        kw = dict(arrowstyle=style, color="k")

        print(center)

        for bottom, s0 in zip([bottom_left, bottom_right], [0,1]):
            arrow_dist = 0.05
            arrow_1 = bottom + np.array([0, arrow_dist]) * SCALE
            arrow_2 = arrow_1 - (np.array([.65, .2])) * SCALE

            p = FancyArrowPatch(
                arrow_1, arrow_2,
                connectionstyle='arc3, rad=-0.2',
                **kw
                )
            ax.add_patch(p)

            ax.text(
                bottom[0], bottom[1]-5,
                r'$s={0}$'.format(s0),
                ha='center',
                va='bottom',
                fontsize=14
            )
        
        ax.set_axis_off()

        fig.savefig('system_marks_{0}.png'.format(usemode))
        plt.close(fig)


    # plotting field at border
    if True:
        print('\t- fields')
        
        center = np.array(data_ez.shape) * .5
        
        fig, ax = plt.subplots(figsize=(10,3))

        # field
        ax.plot(
            s_list,
            field_mid,
            # marker='o',
            zorder=3,
            color='black'
            )
        
        ax.fill_between(
            s_list,
            field_inner,
            field_outer,
            zorder=2,
            color='tab:blue'
            )
        
        # derivative
        ax.plot(
            s_list,
            field_derivative,
            color='red',
            zorder=3
            )

        ax.set_xlim(0,2)
        ax.set_xticks([0,1,2])
        ax.set_yticks([0])

        ax.axvline(1, linestyle='-', color='black', zorder=3)

        for x in [0.75, 1.25]:
            ax.axvline(
                x,
                linestyle='--',
                color='black',
                zorder=1
                )

        ax.set_xlabel(r'$s$')
        ax.set_ylabel(r'$E_z$ / $\frac{\partial E_z}{\partial r}$ [a.u.]')

        fig.savefig('field_{0}_{1}.png'.format(usemode, useside))
        plt.close(fig)


def circle(center, radius, n_points=200):
    '''
    gives point on the surface of a circle with a certain radius
    and around a certain center 
    '''

    phi_list = np.linspace(0, 1, n_points+1)[:-1] * 2. * np.pi + np.pi * 0.5
    p_list = [[np.cos(phi), np.sin(phi)] for phi in phi_list]
    p_list = np.array(p_list) * radius + center

    return p_list


if __name__ == '__main__':
    main()