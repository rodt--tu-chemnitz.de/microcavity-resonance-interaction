set -e

# usemode='antisymmetric'
usemode_list='symmetric antisymmetric'
useside_list='inner outer'

for usemode in $usemode_list; do
    echo $usemode
    echo

    for useside in $useside_list; do
        echo "-> $useside"
        python extract_data.py $usemode $useside
        python calc_husimi.py $usemode $useside
        echo

    done

    echo
done