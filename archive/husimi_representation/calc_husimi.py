import numpy as np
from numpy import fft
import os
import pandas as pd

import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

import sys
sys.path.append('../..')
import python.plotparams

def main():
    '''
    Display data and how the functions are applied
    '''
    s_list, field_list, derivative_list = np.loadtxt('border.dat')

    s0 = 0.9
    p0 = 0.9

    coherent_list = coherent_state(s_list, s0, p0)

    fig, ax = plt.subplots()  


    ax.plot(
        s_list,
        np.real(coherent_list),
        label='coherent state'
        )
    
    ax.plot(
        s_list,
        field_list,
        label='field'
        )
    
    ax.plot(
        s_list,
        np.real(coherent_list * field_list),
        label='coherent * field'
        )

    ax.plot(
        s_list,
        np.real(coherent_list * derivative_list),
        label='coherent * derivative'
        )
    
    ax.axvline(1,linestyle='-', color='black')

    ax.set_xticks([0,1,2])
    ax.set_yticks([0])

    ax.set_xlim(0,2)
    
    ax.legend()

    fig.savefig('coherent_state.png')



k0 = 0.62
sigma = .01
def coherent_state(s, s0, p0):
    '''
    coherent state around s0 and p0
    '''

    len_s = s.shape[0]

    # left circle
    if s0 < 1.:
        bool_list = s > 1.
    # right circle
    else:
        bool_list = s < 1.

    factor_list = np.ones(len_s)
    factor_list[bool_list] = 0


    l_list = [-2, -1, 0, 1, 2]
    # l_list = [0]

    expr_list = []
    for l in l_list:

        expr = np.exp(- np.power(s+l-s0,2) / (2 * sigma) - 1j * k0 * p0 * (s+l))
        expr_list.append(expr)
    
    out = np.power(sigma * np.pi, -0.25) * np.sum(expr_list, axis=0)

    out = out * factor_list

    return out



def main_fft():
    usemode = sys.argv[1]
    useside = sys.argv[2]

    '''
    perform fft of all border functions with gaussian coherent
    state multiplied to them -> fft yields best results so far
    '''

    print('loading data...')
    s_list, field_list, derivative_list = np.loadtxt('border.dat')

    print('seperation by circles...')

    # get left circle
    bool_left = s_list < 1.
    s_left = s_list[bool_left]
    field_left = field_list[bool_left]
    derivative_left = derivative_list[bool_left]

    # get right_circle
    bool_right = np.logical_not(bool_left)
    s_right = s_list[bool_right]
    field_right = field_list[bool_right]
    derivative_right = derivative_list[bool_right]



    # perform fft
    y_fft_field_list = []
    y_fft_der_list = []

    s0_list = np.linspace(0,1,51)[:-1]

    if useside == 'inner':
        n = 3.4
    elif useside == 'outer':
        n = 1
    else:
        print('useside {0} not recognized -> exiting'.format(useside))
        return 0
    
    fcen_low = 0.617718795359919 # symmetric
    fcen_high = 0.622880231053805 # antisymmetric

    if usemode == 'symmetric':
        f = fcen_low
    elif usemode == 'antisymmetric':
        f = fcen_high
    else:
        print('usemode {0} not recognized -> exiting'.format(usemode))
        return 0

    k_max = 2 * np.pi * f * n

    for s, field, der, i in zip([s_left, s_right], [field_left, field_right], [derivative_left, derivative_right], [0,1]):
        
        # angular momentum weighting factor
        samplingrate = np.mean(s[1:] - s[:-1])
        p_list = fft.fftshift(fft.fftfreq(s.shape[0], d=samplingrate)) / k_max
        xi_list = np.arcsin(p_list)
        weighting_factor_list = np.sqrt( n * np.cos(xi_list) )

        # print(np.array([p_list, xi_list / np.pi, weighting_factor_list, 1./weighting_factor_list]).T)

        for s0 in s0_list:
            # field
            y_list = field * gaussian(s - i, s0)
            y_fft = fft.fftshift(fft.fft(y_list)) * weighting_factor_list
            y_fft_re = np.abs(y_fft)
            y_fft_field_list.append(y_fft_re)



            # derivative
            y_list = der * gaussian(s - i, s0)
            y_fft = fft.fftshift(fft.fft(y_list)) / (weighting_factor_list * 2 * np.pi * f)
            y_fft_re = np.abs(y_fft)
            y_fft_der_list.append(y_fft_re)

    y_fft_field_list = np.array(y_fft_field_list)
    y_fft_der_list = np.array(y_fft_der_list)



    # combining both husimi functions

    H_listlist = np.power(np.abs(y_fft_field_list + 1j * y_fft_der_list), 2)



    # plotting

    print('plotting...')


    samplingrate = np.mean(s[1:] - s[:-1])
    print('rate = ', samplingrate)
    print('s.shape = ', s.shape)
    freq_list = fft.fftshift(fft.fftfreq(s.shape[0], d=samplingrate)) / k_max

    freq_min = np.min(freq_list)
    freq_max = np.max(freq_list)

    # print(freq_min, '\t',freq_max)

    for y, name in zip([y_fft_field_list, y_fft_der_list, H_listlist], ['field', 'der', 'combined']):

        fig, ax = plt.subplots(
            figsize=(4,2)
            )



        ax.imshow(
            # np.concatenate((s0_list, s0_list+1)),
            # freq_list,
            np.transpose(y),
            extent=[0,2,-freq_max,-freq_min], # weird order of freqs, so (1,0) is in the center
            cmap='magma',
            aspect='auto'
            )
        
        for x in [0.75, 1.25]:
            ax.axvline(
                x,
                linestyle='--',
                color='white'
                )
        
        for y in [-1/n, 1/n]:
            ax.axhline(
                y,
                linestyle='--',
                color='white'
                )

        ax.set_ylim(-1,1)

        ax.set_xticks([0, 0.75, 1, 1.25, 2])
        ax.set_xticklabels([0, 0.75, 1, 1.25, 2])

        # ax.set_yticks([-1, - 10/k_max, 0, 10/k_max, 1]) # mark theoretical position
        # ax.set_yticklabels(['-1', '-$m/k$', 0, '$m/k$', 1])

        ax.set_yticks([-1, -1/n, 0, 1/n, 1]) # mark theoretical position
        ax.set_yticklabels(['-1', r'-$\chi_\mathrm{C}$', 0, r'$\chi_\mathrm{C}$', 1])

        ax.tick_params(
            direction='inout'
            )

        ax.set_xlabel(r'$s$')
        ax.set_ylabel(r'$\sin \chi$')

        fig.savefig('main_fft_{0}_{1}_{2}.png'.format(usemode, useside, name))
        plt.close(fig)



sigma_2 = 0.007
def gaussian(s, s0):
    '''
    gaussian around s0
    '''
    l_list = [-1, 0, 1]
    # l_list = [0]

    expr_list = []
    for l in l_list:

        expr = np.exp(- np.power(s+l-s0,2) / (2 * sigma_2))
        expr_list.append(expr)
    
    out = np.power(sigma * np.pi, -0.25) * np.sum(expr_list, axis=0)

    return out



if __name__ == '__main__':
    main()
    main_fft()