'''
This file aims to find maxima in the fourier spectrum
in order to find the eigenmodes corresponding to the
eigenfrequencies.

Harminv could also be used to extract resonances
'''

import json
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
from scipy.signal.filter_design import sosfreqz

import plotparams

def main():
    # load json
    with open('parameters.json') as f:
        input = json.load(f)

    print('- reading json')

    RESOLUTION = input['RESOLUTION']
    R = input['R']
    SAMPLINGRATE = input['SAMPLINGRATE']


    # select params
    df = 0.01
    DIST = 0.10

    # reading field data to apply band passes
    filename_field = 'results/df={0:.2f}/data/field/multiple_resonators-DIST={1:.2f}-ez.dat'.format(df, DIST)

    data_field = np.loadtxt(filename_field)
    t, ez_L, ez_R = data_field

    t_min = 5
    t_bool = t > t_min

    t = t[t_bool]
    ez_L = ez_L[t_bool]

    # fft

    ez_L_fft = np.fft.fft(ez_L)
    ez_L_fft = np.fft.fftshift(ez_L_fft)

    freqs = np.fft.fftfreq(len(ez_L_fft), d=SAMPLINGRATE)
    freqs = np.fft.fftshift(freqs)

    freqs_bool = freqs > 0
    freqs = freqs[freqs_bool]
    ez_L_fft = ez_L_fft[freqs_bool]


    # obtain frequencies with highest coefficients
    sortindizes = np.argsort(np.abs(ez_L_fft))

    peak_freqs = freqs[[ sortindizes[-1], sortindizes[-3]]]
    peak_freqs = sorted(peak_freqs)

    print('peak freqs = {0}'.format(peak_freqs))
    print('splitting = {0}'.format(peak_freqs[1] - peak_freqs[0]))


    # obtain filter

    signal_devide = 0.6206882714301404
    order = 300
    fs = 1/SAMPLINGRATE

    sos_low = signal.butter(
        order,                 # order of filter 
        signal_devide,    # frequencies inside bandpass
        btype='low',   # type
        fs=fs,  # sampling frequency
        output='sos'        # 2nd order filtering
        )
    ez_L_filtered_low = signal.sosfilt(
        sos_low,
        ez_L
        )
    
    sos_high = signal.butter(
        order,                 # order of filter 
        signal_devide,    # frequencies inside bandpass
        btype='high',   # type
        fs=fs,  # sampling frequency
        output='sos'        # 2nd order filtering
        )
    ez_L_filtered_high = signal.sosfilt(
        sos_high,
        ez_L,
        )


    # frequency response from filter

    fig, ax = plt.subplots(
        figsize=(5,4)
    )

    # plotting fft
    plot_fft = np.abs(ez_L_fft)
    ax.plot(
        freqs,
        plot_fft / np.max(plot_fft),
        linestyle='dashed',
        label='fourier transform\nof signal',
        color='black'
    )

    # plotting filters
    w_low, h_low = signal.sosfreqz(
        sos_low,
        fs=fs
        )
    ax.plot(
        w_low,
        np.abs(h_low),
        linewidth=4.,
        label='low pass'
        )
    
    w_high, h_high = signal.sosfreqz(
        sos_high,
        fs=fs
        )
    ax.plot(
        w_high,
        np.abs(h_high),
        linewidth=4.,
        label='high pass'
        )


    for f in peak_freqs:
        ax.axvline(
            f,
            zorder=0,
            color='red',
            alpha=0.25,
            )

    ax.set_xlim(0.6, 0.65)
    ax.set_ylim(0, 1.05)

    ax.set_xlabel('$f$')
    ax.set_ylabel('fourier coefficients [a.u.] / frequency response')

    ax.set_yticks([])

    ax.legend()

    fig.savefig('response.png')
    plt.close(fig)



    # plotting field

    fig, ax = plt.subplots()

    alpha = 0.5

    ax.plot(
        t,
        ez_L / np.linalg.norm(ez_L),
        alpha=alpha,
        label='data'
        )

    ax.plot(
        t,
        ez_L_filtered_low / np.linalg.norm(ez_L_filtered_low),
        alpha=alpha,
        label='low pass'
        )
    ax.plot(
        t,
        ez_L_filtered_high / np.linalg.norm(ez_L_filtered_high),
        alpha=alpha,
        label='high pass'
        )
    
    ax.set_xlim(t[0], t[-1])

    ax.legend()

    fig.savefig('findmax_field.png')

if __name__ == '__main__':
    main()