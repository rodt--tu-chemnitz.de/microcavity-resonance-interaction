##################################################
# VARYING DISTS TO ANALYSE ITS INFLUENCE D
##################################################
echo "---- VARYING INTER-CAVITY DISTANCE ----"

# exit if error occurs
set -e

# used dists
dist_list=$(seq 0.010 0.010 0.500 | tr , .) # tr replaces "," with "."
df=0.10
echo "df = $df"
NRES=$1
echo "NRES = $NRES"

for dist in $dist_list; do
    echo
    echo
    sh exec_multires.sh $dist $df $NRES
done


##################################################
# Analyze far fields
##################################################

echo "Analyzing far fields"

# python python/multifile/calc_directionality.py resonances_symmetric
python python/multifile/calc_directionality.py farfield_symmetric
# python python/multifile/calc_directionality.py resonances_antisymmetric
# python python/multifile/calc_directionality.py signal

python python/multifile/plot_directionality.py