set -e

#################################################
# CALCULATING RESONANT MODES OF SINGLE RESONATOR
#################################################

scriptname="singleres_resonances"
mkdir -p singleres

echo "CALCULATING RESONANT MODES OF SINGLE RESONATOR"
echo 
echo "- calculating using meep..."
python python/$scriptname.py > singleres/$scriptname.log

eps_dataname="$scriptname-eps-000000.00.h5"
mv $eps_dataname singleres/$eps_dataname

echo "- saving rersonance results..."
cat singleres/$scriptname.log | grep harminv0 | cut -c 12- > singleres/resonances.csv

#################################################
# CALCULATING RESONANT MODES OF SINGLE RESONATOR
#################################################


scriptname="singleres_bestmode"
mkdir -p singleres

echo "CALCULATING FARFIELD OF BEST MODE OF SINGLE RESONATOR"
echo 
echo "- calculating using meep..."
python python/$scriptname.py > singleres/$scriptname.log

eps_dataname="$scriptname-eps-000000.00.h5"
mv $eps_dataname singleres/$eps_dataname

echo "    -> plotting farfield"
python python/plot_farfield.py singleres/farfield_bestmode.dat singleres/farfield_bestmode.png > /dev/null
