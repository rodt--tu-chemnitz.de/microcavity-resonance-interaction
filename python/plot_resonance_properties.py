'''
plots harminv result csvs

TO BE ACTUALIZED
'''

import numpy as np
import os
import pandas as pd
import sys

import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import plotparams

def main():
    print('PLOTTING PROPERTIES OF RESONANCES')

    # determine relevant frequencies
    f_min = 0.610
    f_max = 0.625
    print('\n-> frequency range: {0:.3f} - {1:.3f}'.format(f_min, f_max))

    N_RESONATORS = int(sys.argv[1])
    print('-> looking for {0} frequencies per D/R due to {0} resonators'.format(N_RESONATORS))

    print('-> reading signal csv')


    # get csvs
    csv_dir = 'csv_multires/'
    csv_names = [f for f in os.listdir(csv_dir) if f.split('_')[-1] == 'signal.csv']
    csv_names = sorted(csv_names)
    

    data_all = []               # contains every eligible resonance
    data_matchfrequencies = []  # contains only data, when the right amount of res. is found
    for f in csv_names:
        # print('\t-reading {0}'.format(f))

        # get DIST
        DIST = float(f.split('_')[1].split('=')[1])

        # read data
        data = pd.read_csv(
            csv_dir + f,
            sep=', ',
            engine='python'
            )
        
        # get bool to determine data to extract
        frequency_list = data['frequency']
        frequency_bool = (f_min < frequency_list) & (frequency_list < f_max)

        Q_list = data['Q']
        Q_bool = 2000. < Q_list
        
        bool_all = frequency_bool & Q_bool
        # check how many frequencies are found
        # we want to find one frequency per microcavity
        n_foundfrequencies = np.count_nonzero(bool_all.values)



        # plotting frequencies
        for data_line, bool in zip(data.values, bool_all.values):
            freq = data_line[0]
            Q = data_line[2]
            amp = data_line[3]

            if bool:
                data_all.append([DIST, freq, Q])

                if n_foundfrequencies == N_RESONATORS:
                    data_matchfrequencies.append([DIST, freq, Q, amp])
        

        
    


    # initializing plot of all frequencies
    print('-> plotting found resonances')

    data_all = np.array(data_all)
    # Q_max = np.max(data_all[:,2])
    Q_max = 5000.

    fig, ax = plt.subplots()

    for line in data_all:
        DIST, freq, Q = line
        
        # negative Q doesn't make sense, yet is calculated
        # don't know why, so just ignore
        if Q < 0 :
            continue

        alpha = Q / Q_max
        if alpha > 1.:
            alpha = 1.
        # print('{0:.2f}, {1:.6f}: {2}'.format(DIST, freq, Q))

        ax.plot(
            DIST,
            freq,
            alpha=alpha,
            marker='o',
            color='black'
            )

    ax.set_ylim(f_min, f_max)
    ax.set_yticks([0.61, 0.615, 0.62])

    ax.set_xlabel(r'$D/R$')
    ax.set_ylabel(r'$f$')

    fig.savefig('images/resonances/signal_harminv_results.png')
    plt.close()


    print('-> evaluating matching resonances')


    # splitting up data per mode
    data_matchfrequencies = np.array(data_matchfrequencies)
    n_datapoints, n_params = data_matchfrequencies.shape
    
    mode_list = [[] for i in range(N_RESONATORS)]
    for i in range(n_datapoints):
        data_line = data_matchfrequencies[i]
        mode_index = i % N_RESONATORS
        mode_list[mode_index].append(data_line)
    mode_list = np.array(mode_list)

    # consistent colors per mode
    color_list = ['C{0}'.format(i) for i in range(N_RESONATORS)]
    ylabel_list = [r'resonant frequency', r'quality factor', r'amplitude']
    

    DIST_list = data_matchfrequencies[:,0]
    Q_list = data_matchfrequencies[:,2]

    for i, ylabel in zip(range(n_params-1), ylabel_list):
        fig, ax = plt.subplots()
        
        for mode_data, color in zip(mode_list, color_list):
            DIST_list = mode_data[:,0]
            y_list = mode_data[:,i+1]

            if i+1 == 2:
                ax.semilogy(
                    DIST_list,
                    y_list,
                    marker='o',
                    color=color
                    )
            else:
                ax.plot(
                    DIST_list,
                    y_list,
                    marker='o',
                    color=color
                    )
                
        ax.set_xlabel(r'$D/R$')
        ax.set_ylabel(ylabel)

        fig.savefig('images/resonances/signal_harminv_param_{0}.png'.format(i))
        plt.close(fig)


if __name__ == '__main__':
    main()