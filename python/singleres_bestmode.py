import meep as mp
import numpy as np
import pandas as pd

import json

import matplotlib.pyplot as plt
import plotparams

import toolbox

def main():
    print('CALCULATE MODE OF SINGLE RESONATOR')

    ###################################
    # parameters
    ###################################

    # load json
    with open('parameters.json') as f:
        input = json.load(f)

    RESOLUTION = input['RESOLUTION']
    N = input['N']
    R = input['R']
    SAMPLINGRATE = input['SAMPLINGRATE']
    DEFORM = input['deform']
    FCEN = input['main_freq']
    FIELD_DECAY_COND = input['field_decay_cond']
    T_MAX = input['T_MAX']

    DPML = 1
    pml_layers = [mp.PML(DPML)]

    PAD = .5

    cell_diameter_nodpml = 2. * (PAD + toolbox.limacon_max_x(R, DEFORM))
    cell_diameter_nodpml = np.ceil(cell_diameter_nodpml)
    cell_diameter_total = cell_diameter_nodpml + 2. * DPML


    cell_size_nodpml = mp.Vector3(cell_diameter_nodpml, cell_diameter_nodpml)
    cell_size = mp.Vector3(cell_diameter_total, cell_diameter_total)

    ###################################
    # geometry
    ###################################

    # parts

    # use circles if no deformity is input -> better performance
    if DEFORM == 0.0:
        cylinder = mp.Cylinder(
            radius=R, 
            material=mp.Medium(index=N)
            )
        geometry = [cylinder]
    else:
        offset_y = np.mean([v.y for v in toolbox.limacon_edges(R, DEFORM)])

        prism = toolbox.limacon_prism(
            radius=R,
            deform=DEFORM,
            center=mp.Vector3(0,-offset_y),
            material=mp.Medium(index=N)
            )
        geometry = [prism]

    ###################################
    # source
    ###################################

    # get
    resonance_data = pd.read_csv(
        'singleres/resonances.csv',
        sep=', ',
        engine='python'
        )

    highQ_index = resonance_data['Q'].idxmax()
    highQ_mode = resonance_data.loc[highQ_index]
    highQ_freq = highQ_mode['frequency']
    highQ_Q = highQ_mode['Q']
    print('selected mode:')
    print('\tfreq:\t{0}'.format(highQ_freq))
    print('\tQ:\t{0}'.format(highQ_Q))

    # source starts on left side of left cavity
    fcen = highQ_freq # pulse center frequency

    source_center_y = R * 0.9
    source_center_vec = mp.Vector3(0, source_center_y)
    
    src_gaussian = mp.Source(
        mp.GaussianSource(fcen, fwidth=0.01),
        component=mp.Ez,
        center=source_center_vec
        )

    sources = [src_gaussian]

    ###################################
    # define sim
    ###################################

    sim = mp.Simulation(
        cell_size=cell_size,
        geometry=geometry,
        sources=sources,
        resolution=RESOLUTION,                    
        boundary_layers=pml_layers,
        # force_complex_fields=True,
        # symmetries=[mp.Mirror(mp.Y,phase=-1)]
        )
    


    ###################################
    # far field preperations
    ###################################

    # define nearfield box for far field calculation
    pos = cell_diameter_nodpml * .5
    nearfield_box = sim.add_near2far(
        fcen,0,1,
        mp.Near2FarRegion( # down
            center=mp.Vector3(0, -pos),
            size=mp.Vector3(cell_diameter_nodpml, 0),
            weight=-1
            ),
        mp.Near2FarRegion( # up
            center=mp.Vector3(0, pos),
            size=mp.Vector3(cell_diameter_nodpml, 0),
            weight=+1
            ),
        mp.Near2FarRegion( # left
            center=mp.Vector3(-pos, 0),
            size=mp.Vector3(0, cell_diameter_nodpml),
            weight=-1
            ),
        mp.Near2FarRegion( # right
            center=mp.Vector3(pos, 0),
            size=mp.Vector3(0, cell_diameter_nodpml),
            weight=+1
            )
        )
    
    flux_box = sim.add_flux(
        fcen, 0, 1,
        mp.FluxRegion( # down
            center=mp.Vector3(0, -pos),
            size=mp.Vector3(cell_diameter_nodpml, 0),
            weight=-1
            ),
        mp.FluxRegion( # up
            center=mp.Vector3(0, pos),
            size=mp.Vector3(cell_diameter_nodpml, 0),
            weight=+1
            ),
        mp.FluxRegion( # left
            center=mp.Vector3(-pos, 0),
            size=mp.Vector3(0, cell_diameter_nodpml),
            weight=-1
            ),
        mp.FluxRegion( # right
            center=mp.Vector3(pos, 0),
            size=mp.Vector3(0, cell_diameter_nodpml),
            weight=+1
            )
        )

    ###################################
    # run sim
    ###################################

    # # save fields

    # fields = []

    # def get_fields(sim):
    #     values = sim.get_array(
    #         center=mp.Vector3(), 
    #         size=cell_size_nodpml,
    #         component=mp.Ez
    #         )
    #     fields.append(values)

    # RUN!
    
    sim.run(
        mp.at_beginning(mp.output_epsilon),
        # mp.at_every(100/fcen, get_fields),
        # mp.to_appended('ez', mp.at_every(SAMPLINGRATE, mp.output_efield_z)),
        # mp.after_sources(mp.Harminv(mp.Ez, source_center_vec, fcen, df)),
        # until_after_sources=T_MAX
        until_after_sources=mp.stop_when_fields_decayed(100, mp.Ez, source_center_vec, FIELD_DECAY_COND)
        )
    
    # fields = np.array(fields)
    # print(fields.shape)

    # np.savetxt('singleres/fields.dat', fields)

    # sim.init_sim()
    # sim.solve_cw(
    #     L=10,
    #     # maxiters=100,
    #     tol=1e-6
    #     )


    ###################################
    # calculating far field copied from
    # https://github.com/NanoComp/meep/blob/master/python/examples/antenna-radiation.py
    ###################################

    print('\ncalculating far field...')


    near_flux = mp.get_fluxes(flux_box)[0]

    r = 1000/fcen      # half side length of far-field square box OR radius of far-field circle
    res_ff = 1         # resolution of far fields (points/μm)
    far_flux_box = (
        nearfield_box.flux(mp.Y, mp.Volume(center=mp.Vector3(y=r), size=mp.Vector3(2*r)), res_ff)[0]
        - nearfield_box.flux(mp.Y, mp.Volume(center=mp.Vector3(y=-r), size=mp.Vector3(2*r)), res_ff)[0]
        + nearfield_box.flux(mp.X, mp.Volume(center=mp.Vector3(r), size=mp.Vector3(y=2*r)), res_ff)[0]
        - nearfield_box.flux(mp.X, mp.Volume(center=mp.Vector3(-r), size=mp.Vector3(y=2*r)), res_ff)[0])

    npts = 100         # number of points in [0,2*pi) range of angles
    angles = np.arange(npts) * 2 * np.pi/npts

    E = np.zeros((npts,3),dtype=np.complex128)
    H = np.zeros((npts,3),dtype=np.complex128)
    for n in range(npts):
        ff = sim.get_farfield( # farfield in direction of Vector3
            nearfield_box,
            mp.Vector3(r*np.cos(angles[n]),r*np.sin(angles[n])) # phi = 0 points up, then counter-clockwise
            )
        E[n,:] = [np.conj(ff[j]) for j in range(3)]
        H[n,:] = [ff[j+3] for j in range(3)]

    Px = np.real(E[:,1]*H[:,2]-E[:,2]*H[:,1])
    Py = np.real(E[:,2]*H[:,0]-E[:,0]*H[:,2])
    Pr = np.sqrt(np.square(Px)+np.square(Py))

    far_flux_circle = np.sum(Pr)*2*np.pi*r/len(Pr)

    print('\nflux:')
    print('\tnear flux:\t\t{0:.6f}'.format(near_flux))
    print('\tfar flux box:\t\t{0:.6f}'.format(far_flux_box))
    print('\tfar flux circle:\t{0:.6f}'.format(far_flux_circle))

    farfield_data = np.array([angles, Pr])
    np.savetxt('singleres/farfield_bestmode.dat', farfield_data)


    ###################################
    # plotting systen
    ###################################

    print('\nplotting...')

    resolution_correction =  0.5 / RESOLUTION
    extent=[
        - (cell_size_nodpml.x * .5) - resolution_correction,
        + (cell_size_nodpml.x * .5) + resolution_correction,
        - (cell_size_nodpml.y * .5) - resolution_correction,
        + (cell_size_nodpml.y * .5) + resolution_correction,
        ]

    # geometry

    eps_data = sim.get_array(
        center=mp.Vector3(),
        size=cell_size_nodpml,
        component = mp.Dielectric
        )
    eps_data = np.transpose(eps_data)


    # field

    ez_data = sim.get_array(
        center=mp.Vector3(0,0),
        size=cell_size_nodpml,
        component=mp.Ez
        )
    ez_data = np.transpose(ez_data)

    ez_data = np.transpose(np.real(ez_data))
    ez_max = np.max(ez_data)
    ez_min = np.min(ez_data)
    ez_absmax = np.max(np.abs([ez_max, ez_min]))

    fig, ax = plt.subplots()

    ax.imshow(
        np.transpose(ez_data),
        cmap='coolwarm',
        origin='lower',
        extent=extent,
        vmin=-ez_absmax,
        vmax=ez_absmax
        )
    
    ax.imshow(
        eps_data,
        cmap='binary',
        alpha=.3,
        origin='lower',
        extent=extent
        )
    
    ax.plot(
        source_center_vec.x,
        source_center_vec.y,
        marker='x',
        color='black'
        )
    
    ax.set_axis_off()

    fig.savefig('singleres/ez_bestmode.png')
    plt.close(fig)

    print('DONE!')

    

if __name__ == '__main__':
    main()