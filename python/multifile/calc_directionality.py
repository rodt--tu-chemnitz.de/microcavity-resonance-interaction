'''
Gets folder and calculates the directionality
for all far fields of the chosen mode in it.
'''

import os
import sys

import numpy as np

import matplotlib.pyplot as plt

sys.path.append('..')
import plotparams

def main():
    print('CALCULATE DIRECTIONALITY')

    mode = sys.argv[1]

    plotting = False
    if len(sys.argv) == 3:
        command = sys.argv[2]
        if command == 'PLOT':
            plotting = True
        else:
            print('ERROR: command {0} not recognized')
            raise ValueError

    ##############################################
    # get files
    ##############################################

    print('- get filenames')

    datadir = 'data/farfield/'

    filenames_all = os.listdir(datadir)
    filenames = []
    for f in filenames_all:
        # splitting looks weird, but just isolates the mode
        file_mode = f[9:].split('DIST')[0][:-1]
        if file_mode == mode:
            filenames.append(f)
    
    # sort by dist
    filenames = sorted(
        filenames,
        key=lambda s: float(s[:-4].split('_')[-1].split('=')[1])
        )
    print(filenames)


    ##############################################
    # settings
    ##############################################

    degree_to_rad = np.pi * 2. / 360.

    # degrees forward and backward to calculate directionality
    angle_degrees = 30.
    angle_rad = angle_degrees * degree_to_rad

    center_forward_rad = 0.5 * np.pi
    center_backward_rad = 1.5 * np.pi 
    # angle, at which spectrum is split and shifter due to periodicity
    angle_split = 1.5 * np.pi

    limits_forward = np.array([center_forward_rad-angle_rad, center_forward_rad+angle_rad])
    limits_backward = np.array([center_backward_rad-angle_rad, center_backward_rad+angle_rad])

    print(limits_forward)
    print(limits_backward)

    ##############################################
    # iterating
    ##############################################

    resultlist = []

    for f in filenames:
        # load data
        angles, farfield = np.loadtxt(datadir + f)
        farfield = farfield / np.max(farfield)

        # integrating

        # forward
        bool_forward = (limits_forward[0] < angles) &  (angles < limits_forward[1])
        angles_forward = angles[bool_forward]
        farfield_forward = farfield[bool_forward]
        f_plus = np.trapz(farfield_forward, x=angles_forward)

        # backward
        bool_backward = (limits_backward[0] < angles) &  (angles < limits_backward[1])
        angles_backward = angles[bool_backward]
        farfield_backward = farfield[bool_backward]
        f_minus = np.trapz(farfield_backward, x=angles_backward)

        # total
        f_total = np.trapz(farfield, x=angles)

        f_dir = (f_plus - f_minus) / f_total

        # append to data
        dist = float(f[:-4].split('_')[-1].split('=')[1])
        resultlist.append([dist, f_dir, f_plus, f_minus, f_total])


        # plotting
        if plotting and f == filenames[0]:

            fig, ax = plt.subplots()

            ax.plot(
                angles / np.pi, farfield,
                color='black',
                )
            ax.plot(
                angles_forward / np.pi, farfield_forward,
                color='tab:red',
                label=r'$f_+$'
                )
            ax.plot(
                angles_backward / np.pi, farfield_backward,
                color='tab:blue',
                label=r'$f_-$'
                )

            ax.legend()

            ax.set_xlabel(r'$\varphi$ [$\pi$]')
            ax.set_ylabel(r'far field [a.u.]')

            fig.savefig('plot.png')

    np.savetxt(
        'data/farfield/directionality_{0}.dat'.format(mode),
        np.transpose(resultlist)
        )


if __name__ == '__main__':
    main()