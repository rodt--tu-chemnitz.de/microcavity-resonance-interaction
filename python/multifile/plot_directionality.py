'''
Plots data calculated by calc_directionality.py
'''

import os
import sys

import numpy as np

import matplotlib.pyplot as plt

sys.path.append('..')
import plotparams


def main():
    print('PLOTTING DIRECTIONALITY')


    print('- reading files')


    print('- plotting')

    fig, ax = plt.subplots()

    modes = ['resonances_symmetric', 'farfield_symmetric']
    labels = ['res_sym', 'ff_sym']

    for mode, label in zip(modes, labels):
        try:
            dist, f_tot, f_plus, f_minus = np.loadtxt('data/farfield/directionality_{0}.dat'.format(mode))
        except ValueError: # if data has not been calculated
            continue
        except OSError: # if file not here
            continue

        ax.plot(
            dist, 
            f_tot,
            label=label,
            marker='o'
            )

    
    ax.set_xlabel(r'$D/R$')
    ax.set_ylabel(r'$f_t$')

    ax.axhline(
        0,
        linewidth=1.,
        color='black',
        linestyle='--'
        )
    
    ax.legend(title=r'$x$-axis symmetry')

    fig.savefig('images/farfield/directionality.png')

if __name__ == '__main__':
    main()