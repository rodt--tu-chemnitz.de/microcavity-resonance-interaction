import meep as mp

import pandas as pd
import numpy as np
import sys
import json

import matplotlib.pyplot as plt
import plotparams

import toolbox

def main():
    print('CALCULATE MODE OF SINGLE RESONATOR')

    ###################################
    # parameters
    ###################################

    # load json
    with open('parameters.json') as f:
        input = json.load(f)

    RESOLUTION = input['RESOLUTION']
    N = input['N']
    R = input['R']
    SAMPLINGRATE = input['SAMPLINGRATE']
    T_MAX = input['T_MAX']
    DEFORM = input['deform']
    FIELD_DECAY_COND = input['field_decay_cond']

    # celculate distance between centers
    DIST = float(sys.argv[1])
    limacon_halfdiameter = toolbox.limacon_max_x(R, DEFORM)
    center_dist = DIST + 2. * limacon_halfdiameter

    df = 0.5


    N_CAVITIES = float(sys.argv[3])
    x_list = np.arange(N_CAVITIES) * center_dist
    x_list = x_list - np.mean(x_list)

    # get calculation mode
    # available modes:
    #   resonances -> place many sources and symmetry planes, to
    #       calculate resonance frequencies. only consider even modes
    #   signal -> only y-plane is symmetric, only place sources in the
    #       leftmost resonator

    symmetry = sys.argv[4]
        


    DPML = 1.
    pml_layers = [mp.PML(DPML)]

    PAD = .2

    #
    offset_y = np.mean([v.y for v in toolbox.limacon_edges(R, DEFORM)])

    cell_dx_nodpml = np.ceil( x_list[-1] - x_list[0] + 2 * (PAD + limacon_halfdiameter) )
    cell_dy_nodpml = np.ceil( 2 * (R + PAD) )
    cell_size_nodpml = mp.Vector3(cell_dx_nodpml, cell_dy_nodpml)

    cell_dx = cell_dx_nodpml + 2. * DPML
    cell_dy = cell_dy_nodpml + 2. * DPML
    cell_size = mp.Vector3(cell_dx, cell_dy)

    print('\nparameters:')
    print('\tsymmetry:\t{0}'.format(symmetry))
    print('\tresolution:\t{0}'.format(RESOLUTION))
    print('\trefraction index:\t{0}'.format(N))
    print('\tcavity_radius:\t{0}'.format(R))
    print('\tinter-cavity distance:\t{0}'.format(DIST))
    print('\t# of cavities:\t{0}'.format(N_CAVITIES))
    # print('\t# of timesteps:\t{0}'.format(T_MAX))
    print('\tdeformation:\t{0}'.format(DEFORM))
    print('\tcalc until fields are below:\t{0}'.format(FIELD_DECAY_COND))

    ###################################
    # geometry
    ###################################

    geometry = []

    if DEFORM == 0.0:
        for x in x_list:
            cylinder = mp.Cylinder(
                center=mp.Vector3(x),
                radius=R,
                material=mp.Medium(index=N)
                )
            
            geometry.append(cylinder)
    else:
        for x in x_list:
            prism = toolbox.limacon_prism(
                radius=R,
                deform=DEFORM,
                center=mp.Vector3(x, -offset_y), # so limacons are centered
                material=mp.Medium(index=N)
                )
            geometry.append(prism)
            

    ###################################
    # source
    ###################################

    # get
    resonance_data = pd.read_csv(
        'singleres/resonances.csv',
        sep=', ',
        engine='python'
        )

    highQ_index = resonance_data['Q'].idxmax()
    highQ_mode = resonance_data.loc[highQ_index]
    highQ_freq = highQ_mode['frequency']
    highQ_Q = highQ_mode['Q']
    print('selected mode:')
    print('\tfreq:\t{0}'.format(highQ_freq))
    print('\tQ:\t{0}'.format(highQ_Q))

    # source starts on left side of left cavity
    fcen = highQ_freq # pulse center frequency

    # determine y-pos of sources

    source_center_y = R * 0.85

    sources = []
    # place sources in all cavities from left to center.
    # therefore one mus differentiate between even
    # and odd number of cavities, for only for odd amounts
    # there is a cavity in the center
    maxindex = int(N_CAVITIES / 2)
    if N_CAVITIES % 2 == 1:
        maxindex = maxindex + 1
    source_center_x_list = [x_list[i] for i in range(maxindex)]

    source_center_x_list = np.array(source_center_x_list)
    for source_center_x in source_center_x_list:
        source_center_vec = mp.Vector3(
            x=source_center_x,
            y=source_center_y
            )
        src = mp.Source(
            mp.GaussianSource(fcen, fwidth=df), 
            mp.Ez, 
            center=source_center_vec
            )
        
        
        sources.append(src)





    ###################################
    # define sim
    ###################################

    symmetries = []
    if symmetry == 'symmetric':
        symmetries.append(mp.Mirror(mp.X, phase=+1))# only even modes
    elif symmetry == 'antisymmetric':
        symmetries.append(mp.Mirror(mp.X, phase=-1))# only uneven modes
    else:
        print('ERROR: symmetry {0} not recognized'.format(symmetry))
        raise ValueError


    sim = mp.Simulation(
        cell_size=cell_size,
        geometry=geometry,
        sources=sources,
        resolution=RESOLUTION,                    
        boundary_layers=pml_layers,
        symmetries=symmetries
        )
    
    ###################################
    # far field preperations
    ###################################

    # define nearfield box for far field calculation
    pos_x = cell_dx_nodpml * .5
    pos_y = cell_dy_nodpml * .5
    nearfield_box = sim.add_near2far(
        fcen,0,1,
        mp.Near2FarRegion( # down
            center=mp.Vector3(0, -pos_y),
            size=mp.Vector3(cell_dx_nodpml, 0),
            weight=-1
            ),
        mp.Near2FarRegion( # up
            center=mp.Vector3(0, pos_y),
            size=mp.Vector3(cell_dx_nodpml, 0),
            weight=+1
            ),
        mp.Near2FarRegion( # left
            center=mp.Vector3(-pos_x, 0),
            size=mp.Vector3(0, cell_dy_nodpml),
            weight=-1
            ),
        mp.Near2FarRegion( # right
            center=mp.Vector3(pos_x, 0),
            size=mp.Vector3(0, cell_dy_nodpml),
            weight=+1
            )
        )
    
    flux_box = sim.add_flux(
        fcen, 0, 1,
        mp.FluxRegion( # down
            center=mp.Vector3(0, -pos_y),
            size=mp.Vector3(cell_dx_nodpml, 0),
            weight=-1
            ),
        mp.FluxRegion( # up
            center=mp.Vector3(0, pos_y),
            size=mp.Vector3(cell_dx_nodpml, 0),
            weight=+1
            ),
        mp.FluxRegion( # left
            center=mp.Vector3(-pos_x, 0),
            size=mp.Vector3(0, cell_dy_nodpml),
            weight=-1
            ),
        mp.FluxRegion( # right
            center=mp.Vector3(pos_x, 0),
            size=mp.Vector3(0, cell_dy_nodpml),
            weight=+1
            )
        )


    ###################################
    # run sim
    ###################################

    # construct ending for files
    savefile_suffix = '_resonances_' + symmetry
    savefile_suffix = savefile_suffix + '_DIST={0:.3f}'.format(DIST)


    # contains source positions of all resonators
    source_center_x_list_all = np.concatenate((source_center_x_list, source_center_x_list * -1.))
    if N_CAVITIES % 2 == 1: # in uneven cases source at zero gets counted twice
        source_center_x_list_all = source_center_x_list_all[:-1]
    source_center_x_list_all = np.sort(source_center_x_list_all)

    # save fields
    fields = []
    x_int_list = [int(x)+1 for x in (source_center_x_list_all + cell_dx_nodpml * .5) * RESOLUTION]

    # THIS STAYS HERE AS A WARNING
    # 
    # idea: all needed get points in sim-coordinates
    # and ask sim nicely for field components
    # 
    # problem: fields which were accessed later in
    # for loop mysteriously returned lower values
    #
    # -> a flip of the order of the x coordinates confirmed
    #    suspicion, but no idea what caused it
    #
    # def get_fields(sim):
    #     sublist = []
    #     for x in source_center_x_list_all:
    #         value = sim.get_array(
    #             center=mp.Vector3(x, source_center_y), 
    #             size=mp.Vector3(),
    #             component=mp.Ez
    #             )
    #         sublist.append(value)
    #     print('{0}'.format(np.array(sublist)))
    #     fields.append(sublist)
    
    # get fields at source points
    # get field array at top with get_array, extract further points
    # by using x-coords of sources converted to their indices.
    # not 100% exact due to snapping to points of the yee grid without
    # interpolation, but should be exact enough
    def get_fields(sim):
        sublist = []
        # read all fields at required height
        field_array = sim.get_array(
            center=mp.Vector3(0, source_center_y), 
            size=mp.Vector3(cell_dx_nodpml, 0),
            component=mp.Ez
            )
        
        for x in x_int_list:
            x_int = int(x)
            value = field_array[x_int]
            sublist.append(value)


        fields.append(sublist)



    
    # evaluate Harminv at leftmost point
    Harminv_loc = mp.Vector3(source_center_x_list[0], source_center_y)

    # calculate until the fields are decayed
    sim.run(
        mp.at_beginning(mp.output_epsilon),
        # mp.to_appended('ez', mp.at_every(SAMPLINGRATE, mp.output_efield_z)),
        mp.at_every(SAMPLINGRATE, get_fields),
        mp.after_sources(mp.Harminv(mp.Ez, Harminv_loc, fcen, df)),
        until=T_MAX
        # until_after_sources=mp.stop_when_fields_decayed(50, mp.Ez, source_center_vec, FIELD_DECAY_COND)
        )

    # saving fields
    fields = np.transpose(fields)
    np.savetxt(
        'data/fields/fields' + savefile_suffix + '.dat',
        fields
        )
    
    ###################################
    # calculating far field copied from
    # https://github.com/NanoComp/meep/blob/master/python/examples/antenna-radiation.py
    ###################################

    print('\ncalculating far field...')


    near_flux = mp.get_fluxes(flux_box)[0]

    r = 1000/fcen      # half side length of far-field square box OR radius of far-field circle
    res_ff = 1         # resolution of far fields (points/μm)
    far_flux_box = (
        nearfield_box.flux(mp.Y, mp.Volume(center=mp.Vector3(y=r), size=mp.Vector3(2*r)), res_ff)[0]
        - nearfield_box.flux(mp.Y, mp.Volume(center=mp.Vector3(y=-r), size=mp.Vector3(2*r)), res_ff)[0]
        + nearfield_box.flux(mp.X, mp.Volume(center=mp.Vector3(r), size=mp.Vector3(y=2*r)), res_ff)[0]
        - nearfield_box.flux(mp.X, mp.Volume(center=mp.Vector3(-r), size=mp.Vector3(y=2*r)), res_ff)[0])

    npts = 200         # number of points in [0,2*pi) range of angles
    angles = np.arange(npts) * 2 * np.pi/npts

    E = np.zeros((npts,3),dtype=np.complex128)
    H = np.zeros((npts,3),dtype=np.complex128)
    for n in range(npts):
        ff = sim.get_farfield( # farfield in direction of Vector3
            nearfield_box,
            mp.Vector3(r*np.cos(angles[n]),r*np.sin(angles[n])) # phi = 0 points up, then counter-clockwise
            )
        E[n,:] = [np.conj(ff[j]) for j in range(3)]
        H[n,:] = [ff[j+3] for j in range(3)]

    Px = np.real(E[:,1]*H[:,2]-E[:,2]*H[:,1])
    Py = np.real(E[:,2]*H[:,0]-E[:,0]*H[:,2])
    Pr = np.sqrt(np.square(Px)+np.square(Py))

    far_flux_circle = np.sum(Pr)*2*np.pi*r/len(Pr)

    print('\nflux:')
    print('\tnear flux:\t\t{0:.6f}'.format(near_flux))
    print('\tfar flux box:\t\t{0:.6f}'.format(far_flux_box))
    print('\tfar flux circle:\t{0:.6f}'.format(far_flux_circle))

    farfield_data = np.array([angles, Pr])
    np.savetxt('data/farfield/farfield' + savefile_suffix + '.dat', farfield_data)




    ###################################
    # plotting systen
    ###################################

    print('\nplotting...')

    resolution_correction =  0.5 / RESOLUTION
    extent=[
        - (cell_size_nodpml.x * .5) - resolution_correction,
        + (cell_size_nodpml.x * .5) + resolution_correction,
        - (cell_size_nodpml.y * .5) - resolution_correction,
        + (cell_size_nodpml.y * .5) + resolution_correction,
        ]

    # geometry

    eps_data = sim.get_array(
        center=mp.Vector3(),
        size=cell_size_nodpml,
        component = mp.Dielectric
        )
    eps_data = np.transpose(eps_data)


    # field

    ez_data = sim.get_array(
        center=mp.Vector3(0,0),
        size=cell_size_nodpml,
        component=mp.Ez
        )
    ez_data = np.transpose(ez_data)

    np.savetxt('data/ez/ez-' + savefile_suffix + '.dat', ez_data)
    
    fig, ax = plt.subplots()

    ax.imshow(
        ez_data,
        cmap='coolwarm',
        origin='lower',
        # extent=extent
        )
    
    ax.imshow(
        eps_data,
        cmap='binary',
        alpha=.3,
        origin='lower',
        # extent=extent
        )


    for x_int in x_int_list:
        y_int = int( (source_center_y + cell_dy_nodpml * 0.5) * RESOLUTION )
        ax.plot(
            x_int,
            y_int,
            marker='x',
            color='black'
            )
    
    ax.set_axis_off()

    fig.savefig('images/ez/ez_data' + savefile_suffix + '.png')
    plt.close(fig)

    print('DONE!')
    

if __name__ == '__main__':
    main()