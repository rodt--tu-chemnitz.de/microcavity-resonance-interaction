import sys
import numpy as np

import matplotlib.pyplot as plt
import plotparams

def main():
    print('PLOTTING FARFIELD')

    input = sys.argv[1]
    output = sys.argv[2]

    print('\nloading data...')
    print('\t' + input)

    data = np.loadtxt(input)

    print('plotting...')
    print('\t' + output)

    fig, ax = plt.subplots(
        subplot_kw=dict(projection='polar')
    )

    ax.plot(
        data[0],
        data[1] / np.max(data[1])
        )
    
    xticks = np.linspace(0, 1.5, 4) * np.pi
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticks/np.pi)

    ax.set_yticks([0.5, 1])
    ax.set_ylim(0,1)

    ax.set_xlabel(r'angle [$\pi$]')

    fig.savefig(output)

    print('DONE!')


if __name__ == '__main__':
    main()