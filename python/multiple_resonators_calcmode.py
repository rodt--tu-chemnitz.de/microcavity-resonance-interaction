'''
calculates resonant mode of system

TO BE ACTUALIZED
'''

import meep as mp

import pandas as pd
import numpy as np
import sys
import json

import matplotlib.pyplot as plt
import plotparams


def main():
    print('CALCULATE MODE OF SINGLE RESONATOR')

    ###################################
    # parameters
    ###################################

    # load json
    with open('parameters.json') as f:
        input = json.load(f)

    RESOLUTION = input['RESOLUTION']
    RESOLUTION = 50
    N = input['N']
    R = input['R']
    SAMPLINGRATE = input['SAMPLINGRATE']
    T_MAX = input['T_MAX']



    DIST = 0.1
    center_dist = 2 * R + DIST

    N_CAVITIES = 2
    x_list = np.arange(N_CAVITIES) * center_dist
    x_list = x_list - np.mean(x_list)


    DPML = 1.
    pml_layers = [mp.PML(DPML)]

    PAD = .1

    cell_dx_nodpml = x_list[-1] - x_list[0] + 2 * (R + PAD)
    cell_dy_nodpml = 2 * (R + PAD)
    vol_nodpml = mp.Volume(mp.Vector3(), size=mp.Vector3(cell_dx_nodpml, cell_dy_nodpml))

    cell_dx = cell_dx_nodpml + 2* DPML
    cell_dy = cell_dy_nodpml + 2* DPML
    cell_size = mp.Vector3(cell_dx, cell_dy)

    print('\nparameters:')
    print('\tresolution:\t{0}'.format(RESOLUTION))
    print('\trefraction index:\t{0}'.format(N))
    print('\tcavity_radius:\t{0}'.format(R))
    print('\tinter-cavity distance:\t{0}'.format(DIST))
    print('\t# of cavities:\t{0}'.format(N_CAVITIES))
    print('\t# of timesteps:\t{0}'.format(T_MAX))

    ###################################
    # geometry
    ###################################

    geometry = []

    for x in x_list:
        cyl = mp.Cylinder(
            center=mp.Vector3(x=x),
            radius=R,
            material=mp.Medium(index=N)
            )
        geometry.append(cyl)

    ###################################
    # source
    ###################################

    # source starts on left side of left cavity
    source_center_x = x_list[0]
    source_center_y = - 0.9 * R
    source_center_vec = mp.Vector3(
        x=source_center_x,
        y=source_center_y
        )


    use_symmetric = True

    fcen_low = 0.617718795359919 # symmetric
    fcen_high = 0.622880231053805 # antisymmetric
    
    if use_symmetric:
        fcen = fcen_low
    else:
        fcen = fcen_high

    src = mp.Source(
        mp.ContinuousSource(fcen, width=0.5), 
        mp.Ez, 
        center=source_center_vec
        )

    sources = [src]

    ###################################
    # define sim
    ###################################

    symmetries = [
        mp.Mirror(mp.Y, phase=+1)
        ]
    
    if use_symmetric:
        symmetries.append(mp.Mirror(mp.X, phase=+1))
    else:
        symmetries.append(mp.Mirror(mp.X, phase=-1))

    sim = mp.Simulation(
        cell_size=cell_size,
        geometry=geometry,
        sources=sources,
        resolution=RESOLUTION,                    
        boundary_layers=pml_layers,
        symmetries=symmetries,
        force_complex_fields=True
        )
    

    ###################################
    # perform frequency domain calculation
    ###################################

    sim.init_sim()

    sim.solve_cw(maxiters=10000000, L=10)
    # sim.run(until=10)
    
    ###################################
    # obtain results
    ###################################

    print('\n\nObtaining results...')

    print(cell_size)

    eps_data = sim.get_array(component=mp.Dielectric, vol=vol_nodpml)
    
    ez_data = sim.get_array(component=mp.Ez, vol=vol_nodpml)
    ez_data = np.real(ez_data)

    ez_min = np.abs(np.min(ez_data))
    ez_max = np.abs(np.max(ez_data))

    ez_absmax = np.max([ez_min, ez_max])



    ###################################
    # plotting
    ###################################

    print('Plotting...')

    fig, ax = plt.subplots()

    ax.imshow(eps_data.transpose(), interpolation='spline36', cmap='binary')
    ax.imshow(ez_data.transpose(), interpolation='spline36', cmap='RdBu', alpha=0.7, vmin=-ez_absmax, vmax=ez_absmax)

    ax.set_axis_off()

    if use_symmetric:
        suffix = 'symmetric'
    else:
        suffix = 'antisymmetric'

    fig.savefig('mode_{0}.png'.format(suffix))

    # saving data
    print('saving data...')

    np.savetxt(
        'mode_{0}_ez.dat'.format(suffix),
        ez_data
        )
    np.savetxt(
        'mode_{0}_eps.dat'.format(suffix),
        eps_data
        )

if __name__ == '__main__':
    main()