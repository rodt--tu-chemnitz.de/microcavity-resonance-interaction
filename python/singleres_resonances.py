import meep as mp
import numpy as np

import json

import matplotlib.pyplot as plt
import plotparams

import toolbox


def main():
    print('CALCULATE MODE OF SINGLE RESONATOR')

    ###################################
    # parameters
    ###################################

    # load json
    with open('parameters.json') as f:
        input = json.load(f)

    RESOLUTION = input['RESOLUTION']
    N = input['N']
    R = input['R']
    SAMPLINGRATE = input['SAMPLINGRATE']
    DEFORM = input['deform']
    FCEN = input['main_freq']
    FIELD_DECAY_COND = input['field_decay_cond']
    T_MAX = input['T_MAX']

    DPML = 1
    pml_layers = [mp.PML(DPML)]

    PAD = .5

    cell_diameter_nodpml = 2. * (PAD + toolbox.limacon_max_x(R, DEFORM))
    cell_diameter_nodpml = np.ceil(cell_diameter_nodpml)
    cell_diameter_total = cell_diameter_nodpml + 2. * DPML

    cell_size_nodpml = mp.Vector3(cell_diameter_nodpml, cell_diameter_nodpml)
    cell_size = mp.Vector3(cell_diameter_total, cell_diameter_total)

    ###################################
    # geometry
    ###################################

    # parts

    # use circles if no deformity is input -> better performance
    if DEFORM == 0.0:
        cylinder = mp.Cylinder(
            radius=R, 
            material=mp.Medium(index=N),
            center=mp.Vector3()
            )
        geometry = [cylinder]
    else:
        offset_y = np.mean([v.y for v in toolbox.limacon_edges(R, DEFORM)])

        prism = toolbox.limacon_prism(
            radius=R,
            deform=DEFORM,
            center=mp.Vector3(0,-offset_y),
            material=mp.Medium(index=N)
            )
        geometry = [prism]

    ###################################
    # source
    ###################################

    fcen = FCEN              # pulse center frequency
    df = .5                 # pulse frequency width

    source_center_y = R * 0.9
    source_center_vec = mp.Vector3(0, source_center_y)

    src = mp.Source(
        mp.GaussianSource(fcen, fwidth=df), 
        mp.Ez, 
        center=source_center_vec
        )

    sources = [src]


    ###################################
    # define sim
    ###################################

    sim = mp.Simulation(
        cell_size=cell_size,
        geometry=geometry,
        sources=sources,
        resolution=RESOLUTION,                    
        boundary_layers=pml_layers,
        # symmetries=[mp.Mirror(mp.Y,phase=+1)]
        )

    ###################################
    # run sim
    ###################################

    # save fields

    fields = []

    def get_fields(sim):
        values = sim.get_array(
            center=source_center_vec, 
            size=mp.Vector3(),
            component=mp.Ez
            )
        fields.append(values)

    # RUN!
    
    sim.run(
        mp.at_beginning(mp.output_epsilon),
        mp.at_every(SAMPLINGRATE, get_fields),
        # mp.to_appended('ez', mp.at_every(SAMPLINGRATE, mp.output_efield_z)),
        mp.after_sources(mp.Harminv(mp.Ez, source_center_vec, fcen, df)),
        until_after_sources=T_MAX
        )
    
    fields = np.array(fields)
    print(fields.shape)

    np.savetxt('singleres/fields_resonancecalc.dat', fields)

    ###################################
    # plotting systen
    ###################################

    print('\nplotting...')

    resolution_correction =  0.5 / RESOLUTION
    extent=[
        - (cell_size_nodpml.x * .5) - resolution_correction,
        + (cell_size_nodpml.x * .5) + resolution_correction,
        - (cell_size_nodpml.y * .5) - resolution_correction,
        + (cell_size_nodpml.y * .5) + resolution_correction,
        ]

    # geometry

    eps_data = sim.get_array(
        center=mp.Vector3(),
        size=cell_size_nodpml,
        component = mp.Dielectric
        )
    eps_data = np.transpose(eps_data)

    fig, ax = plt.subplots()

    ax.imshow(
        eps_data,
        cmap='binary',
        origin='lower',
        extent=extent
        )
    
    ax.plot(
        source_center_vec.x,
        source_center_vec.y,
        marker='x',
        color='red'
        )
    
    ax.set_axis_off()

    fig.savefig('singleres/eps_resonancecalc.png')
    plt.close(fig)



    # field

    ez_data = sim.get_array(
        center=mp.Vector3(0,0),
        size=cell_size_nodpml,
        component=mp.Ez
        )
    ez_data = np.transpose(ez_data)
    ez_max = np.max(ez_data)
    ez_min = np.min(ez_data)
    

    fig, ax = plt.subplots()

    ax.imshow(
        ez_data,
        cmap='coolwarm',
        origin='lower',
        extent=extent
        )
    
    ax.imshow(
        eps_data,
        cmap='binary',
        alpha=.3,
        origin='lower',
        extent=extent
        )
    
    ax.plot(
        source_center_vec.x,
        source_center_vec.y,
        marker='x',
        color='black'
        )
    
    ax.set_axis_off()

    fig.savefig('singleres/pattern_resonancecalc.png')
    plt.close(fig)

    print('DONE!')

    

if __name__ == '__main__':
    main()