import numpy as np
import json
import sys

import matplotlib.pyplot as plt
import plotparams

def main():
    print('PLOTTING FIELDS')

    #################################
    # reading and preparing data
    #################################

    # load json
    with open('parameters.json') as f:
        params = json.load(f)

    print('- reading json')

    SAMPLINGRATE = params['SAMPLINGRATE']
    
    input = sys.argv[1]
    output = sys.argv[2]

    data = np.loadtxt(input)
    
    # exception handeling for 1-resonator case
    try:
        n_resonators, n_times = data.shape
    except ValueError:
        n_resonators = 1
        n_times = data.shape[0]

        data = np.array([data])


    t_list = np.arange(n_times) * SAMPLINGRATE


    #################################
    # plotting
    #################################


    print('- plotting')

    fig, ax = plt.subplots(figsize=(8,3))

    # maximum of all data to scale
    y_absmax = np.max(np.abs(data))
    # scale is chosen this way so field lines of neighboring resonators
    # don't touch
    scale = 0.5 / y_absmax

    for ii in range(n_resonators):
        y_list = data[ii]

        print('resonator {0}, max field: {1:.6f}'.format(ii+1, np.max(y_list)))
        
        # norming data for pretty plotting
        y_list = y_list * scale

        ax.plot(t_list, y_list + ii + 1)

    # mark different plots
    for ii in range(n_resonators-1):
        ax.axhline(
            ii+1.5,
            color='black',
            linewidth=1.
            )

    ax.set_xlim(t_list[0], t_list[-1])
    ax.set_ylim(0.5, n_resonators+0.5)

    ax.set_yticks(np.arange(n_resonators) + 1)

    ax.set_xlabel(r'$t$')
    ax.set_ylabel(r'$E_z$ [a.u.], microcavity index')

    # ax.set_yticks([0])

    print('- saving')

    fig.savefig(output)

if __name__ == '__main__':
    main()