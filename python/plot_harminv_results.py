'''
plots harminv results
'''

import numpy as np
import os
import pandas as pd

import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import plotparams


def main():
    print('PLOTTING HARMINV RESULTS')


    resultdir = 'results'

    sourcedirs = sorted(
        [f for f in os.listdir(resultdir) if os.path.isdir('{0}/{1}'.format(resultdir, f))],
        )

    print('\nfound sourcedirs: ', sourcedirs, end='\n\n')

    
    # relevant frequencies
    f_min = 0.500
    f_max = 0.625

    fig, ax = plt.subplots()

    color_list = ['C{0}'.format(i) for i in range(10)]
    marker_symmetric = 'X'
    marker_antisymmetric = 'P'
    for sd, color in zip(sourcedirs, color_list):
        print('- {0}'.format(sd))
        csvdir = '{0}/{1}/csv_multires'.format(resultdir, sd)

        for s, marker in zip(['symmetric', 'antisymmetric'], [marker_symmetric, marker_antisymmetric]):
            # reading resonance csvs
            distdirs = []
            for f in os.listdir(csvdir):
                fsplit = f.split('-')
                if fsplit[-1] == 'resonances_{0}.csv'.format(s):
                    distdirs.append(f)
            distdirs = sorted(distdirs)
            
            # lists for the data to be stored
            frequency_listlist = [] # x data
            Q_listlist = [] # alpha data
            dist_list = [] # y data
            
            for dd in distdirs:
                dist = float(dd.split('-')[1].split('=')[1])
                dist_list.append(dist)

                filename_csv = '{0}/{1}'.format(csvdir, dd)
                data = pd.read_csv(
                    filename_csv,
                    sep=', ',
                    engine='python'
                    )
                
                frequency_list = data['frequency'].values
                Q_list = data['Q'].values

                frequency_bool_list = (f_min < frequency_list) & (frequency_list < f_max)

                frequency_listlist.append(frequency_list[frequency_bool_list])
                Q_listlist.append(Q_list[frequency_bool_list])
            

            # Q_max = np.max(np.abs(np.concatenate(Q_listlist)))

            # determine color

            print(Q_listlist)

            for d, f_l, Q_l in zip(dist_list, frequency_listlist, Q_listlist):
                if len(Q_l) == 0:
                    continue

                Q_max = np.max(np.abs(Q_l))

                for f, Q in zip(f_l, Q_l):
                    Q_abs = np.abs(Q)
                    alpha = Q_abs / Q_max
                    # alpha = 1.

                    ax.plot(
                        f, d,
                        alpha=alpha,
                        color=color,
                        marker=marker,
                        markeredgecolor='None'
                        )
    
    legend_elements = []
    for sd, color in zip(sourcedirs, color_list):
        NRES = sd.split('_')[1].split('=')[1]
        NRES = int(NRES)


        l = Line2D(
            [0], [0],
            linestyle='-',
            color=color,
            linewidth=4.,
            label='{0} resonators'.format(NRES)
            )
        
        legend_elements.append(l)


    # append markers to legend
    l = Line2D(
        [0], [0],
        linestyle=' ',
        color='gray',
        marker=marker_symmetric,
        label='symmetric mode'
        )
    legend_elements.append(l)
    l = Line2D(
        [0], [0],
        linestyle=' ',
        color='gray',
        marker=marker_antisymmetric,
        label='antisymmetric mode'
        )
    legend_elements.append(l)


    ax.legend(
        handles=legend_elements,
        # title=r'# of resonators'
        )

    # mark source
    resonance_data = pd.read_csv(
        'singleres/resonances.csv',
        sep=', ',
        engine='python'
        )
    highQ_index = resonance_data['Q'].idxmax()
    highQ_mode = resonance_data.loc[highQ_index]
    highQ_freq = highQ_mode['frequency']
    highQ_Q = highQ_mode['Q']
    fcen = highQ_freq
    print('\nselected mode:')
    print('\tfreq:\t{0}'.format(fcen))
    print('\tQ:\t{0}'.format(highQ_Q))
    ax.axvline(
        fcen,
        zorder=0,
        color='black',
        linestyle='dashed'
        )
    ax.text(
        fcen, 0.05,
        r'$f_\mathrm{src}$',
        ha='center',
        va='center',
        bbox=dict(
            boxstyle="round",
            ec='black',
            fc='white',
            )
        )

    ax.set_xlabel(r'$f$')
    ax.set_ylabel(r'$D/R$')

    ax.set_xlim(f_min, f_max)
    ax.set_ylim(0, 0.5)

    # tickstep = 0.01
    # ax.set_xticks(np.arange(f_min, f_max, tickstep))

    fig.savefig('results/harminv_results.png')

        


if __name__ == '__main__':
    main()