'''
Compares the fourier params for all systems,
also depending on the frequency width


TO BE ACTUALIZED
'''


import numpy as np
import os
import pandas as pd
import json

import plotparams
from matplotlib.patches import Patch
from matplotlib.lines import Line2D
import matplotlib.pyplot as plt

def main():
    # load json
    with open('parameters.json') as f:
        input = json.load(f)

    print('- reading json')

    RESOLUTION = input['RESOLUTION']
    R = input['R']
    SAMPLINGRATE = input['SAMPLINGRATE']
    # params
    savedir = 'results/'

    # initialize plot

    cmaps = ['Reds', 'Blues', 'Greens']
    fig, ax = plt.subplots(
        figsize=(7,3)
        )


    # get result_systems

    resultdir_list = sorted(
        [savedir + f for f in os.listdir(savedir) if os.path.isdir(savedir + f)],
        key=lambda s: float(s.split('=')[-1])
        )


    for resultdir, cmap in zip(resultdir_list, cmaps):
        print(resultdir)


        print('- finding files')


        # getting files
        data_dir = resultdir + '/data/fft/'
        data_filenames = os.listdir(data_dir)
        data_filenames = [f for f in data_filenames if f.split('-')[-1] == 'ez_fft.dat' and f.split('-')[0] == 'multiple_resonators']
        data_filenames = sorted(data_filenames)

        # analyzing distances
        dist_list = [float( f.split('-')[1].split('=')[1] ) for f in data_filenames]

        # initialize big plot

        # analyzing distances
        dist_list = [float( f.split('-')[1].split('=')[1] ) for f in data_filenames]

        dist_min = np.min(dist_list)
        dist_max = np.max(dist_list)
        dist_delta = dist_max - dist_min

        # iterating

        fft_alldata = []
        for f, dist in zip(data_filenames, dist_list):
            print(f)

            if dist == 'inf':
                print('-> skipped!')
                continue

            # remove ending
            f_trunc = f[:-3]

            ####################################################
            # initializing
            ####################################################

            print('- reading data')

            try:
                data = np.loadtxt(data_dir + f)
            except OSError:
                print('-> file not founds')
                continue

            freq_list = data[0]
            fft_left = data[1]
            # normalize
            fft_left = fft_left / np.linalg.norm(fft_left)

            # fft_right = data[2]

            fft_alldata.append(fft_left)

        print('plotting to contourplot')

        # creating x- and y-grids for pcolormas input
        fft_alldata = np.array(fft_alldata)
        n_y, n_x = fft_alldata.shape

        x_onedim = freq_list
        dx = np.mean(x_onedim[1:] - x_onedim[:-1])
        x_onedim = np.concatenate((x_onedim,[x_onedim[-1]+dx])) - dx * .5
        x_grid = np.array([x_onedim for i in range(n_y+1)])
        
        dist_list = dist_list
        y_onedim = np.array(dist_list)
        dy = np.mean(y_onedim[1:] - y_onedim[:-1])
        y_onedim = np.concatenate((y_onedim,[y_onedim[-1]+dy])) - dy * .5
        y_grid = np.array([y_onedim for i in range(n_x+1)]).T

        fft_alldata = np.array(fft_alldata)
        cf = ax.pcolormesh(
            x_grid,
            y_grid,
            fft_alldata,
            cmap=cmap,
            alpha=.5
            )


    
    ax.axvline(
        0.6206882714301404,
        color='red',
        linestyle='--'
        )

    legend_handles = []
    for resultdir, cmap in zip(resultdir_list, cmaps):
        df = float( resultdir.split('=')[-1] )
        colormap = plt.get_cmap(cmap)
        color = colormap(.5)
        print(color)

        p = Patch(
            facecolor=color,
            label='df = {0:.3f}'.format(df)
            )
        legend_handles.append(p)

    l = Line2D(
        [0],[0],
        linestyle='--',
        color='red',
        label='source frequency'
        )
    legend_handles.insert(0, l)

    ax.legend(
        handles=legend_handles,
        loc='upper right'
        )

    ax.set_xlim(0.60, 0.64)
    ax.set_xticks([0.60, 0.61, 0.62, 0.63, 0.64])

    ax.set_xlabel('frequency')
    ax.set_ylabel('intercavity distance $D / R$')

    fig.savefig(savedir + 'fftall_rersultcompare.png')

if __name__ == '__main__':
    main()