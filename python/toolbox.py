from meep import Prism, Vector3
from numpy import sin, cos, pi, linspace, arcsin, sqrt



########################################################
# Limacon functions
########################################################

def limacon_edges(radius, deform, n_edgepoints=50):
    '''
    Returns list of Vector3 containing edgepoints of a limacon
    radius... radius of corresponding circle
    deform... deformation of circle
    n_edgepoints... number of points at the edge to approximate limacon
    '''

    phi_list = linspace(0, 1, n_edgepoints+1)[:-1] * 2. * pi

    vector_list = []
    for phi in phi_list:
        radius_now = radius * (1. + deform * sin(phi)) # offset to turn limacon

        x = radius_now * cos(phi)
        y = radius_now * sin(phi)

        vector_now = Vector3(x,y)
        vector_list.append(vector_now)
    
    return vector_list
    


PRISM_HEIGHT = 1.
def limacon_prism(radius, deform, material, center=None):
    '''
    Returns Prism of limacon
    radius... radius of corresponding circle
    deform... deformation of circle
    material... meep material class
    center... center of mass of limacon
    '''
    vertices = limacon_edges(radius, deform)

    out = Prism(
        vertices=vertices,
        height=PRISM_HEIGHT,
        material=material,
        center=center
        )
    
    return out


def limacon_max_x(radius, deform):
    '''
    Returns maximum x value of limacon
    '''

    # if else clause to avoide deviding by zero
    if abs(deform) < 1e-3:
        return radius
    else:
        arg = ( sqrt(8. * deform**2 + 1.) - 1. ) / (4. * deform)
        phi = arcsin(arg)

        x_max = radius * cos(phi) * (1. + deform * sin(phi))

        return x_max

