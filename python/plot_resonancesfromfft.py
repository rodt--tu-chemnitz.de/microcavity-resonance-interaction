import numpy as np

import matplotlib.pyplot as plt

import plotparams

def main():
    print('PLOTTING FFT RESULTS')

    print('\n-> reading data')
    
    data = np.loadtxt('data/fft/resonances-from-fft.dat')
    try:
        n_resonators = int( (data.shape[1]-1) / 2 )
    except IndexError:
        print('ERROR: Not enough resonances found over all distances for analysis!')
        return
    dist_list = data[:,0]

    frequency_listlist = [data[:,2*i+1] for i in range(n_resonators)]
    Q_listlist = [data[:,2*i+2] for i in range(n_resonators)]


    print('-> plotting resonance frequencies')

    fig, ax = plt.subplots()
    
    for frequency_list in frequency_listlist:
        ax.plot(
            dist_list, 
            frequency_list,
            marker='o'
            )

    ax.set_xlabel(r'$D/R$')
    ax.set_ylabel(r'resonance frequency')

    fig.savefig('images/fft/fftresult-resonances.png')
    plt.close(fig)


    print('-> plotting quality factors')

    fig, ax = plt.subplots()
    
    for Q_list in Q_listlist:
        ax.plot(
            dist_list, 
            Q_list,
            marker='o'
            )

    ax.set_xlabel(r'$D/R$')
    ax.set_ylabel(r'quality factor')

    fig.savefig('images/fft/fftresult-Q.png')
    plt.close(fig)


    print('-> plotting composite values')

    fig, ax = plt.subplots()
    
    for frequency_list, Q_list in zip(frequency_listlist, Q_listlist):
        ax.plot(
            frequency_list, 
            Q_list,
            marker='o',
            linestyle=' ',
            )
        
    ax.set_xlabel(r'resonance frequency')
    ax.set_ylabel(r'quality factor')

    fig.savefig('images/fft/fftresult-composite.png')
    plt.close(fig)

if __name__ == '__main__':
    main()