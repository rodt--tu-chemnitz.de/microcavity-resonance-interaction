import numpy as np
import json
import sys
import pandas as pd

import plotparams
import matplotlib.pyplot as plt

def main():
    print('PLOTTING FFT')
    
    # params

    input = sys.argv[1]
    output = sys.argv[2]


    ####################################################
    # read
    ####################################################

    print('- reading json')

    # load json
    with open('parameters.json') as f:
        params = json.load(f)

    SAMPLINGRATE = params['SAMPLINGRATE']

    print('- reading csv')
    # get
    resonance_data = pd.read_csv(
        'singleres/resonances.csv',
        sep=', ',
        engine='python'
        )

    highQ_index = resonance_data['Q'].idxmax()
    highQ_mode = resonance_data.loc[highQ_index]
    highQ_freq = highQ_mode['frequency']
    highQ_Q = highQ_mode['Q']
    print('selected mode:')
    print('\tfreq:\t{0}'.format(highQ_freq))
    print('\tQ:\t{0}'.format(highQ_Q))


    print('- reading data')

    data = np.loadtxt(input)
    n_resonators = data.shape[0] - 1

    frequencies = data[0]


    ####################################################
    # plotting
    ####################################################

    fig, ax = plt.subplots(figsize=(6,3))

    coeff_max = np.max(data[1:])

    for ii in range(n_resonators):
        coefficients = data[ii+1]
        ax.plot(
            frequencies,
            coefficients / coeff_max,
            label=ii+1,
            zorder=2
            )
    
    ax.axvline(
        highQ_freq,
        color='gray',
        zorder=1
        )

    ax.legend(title=r'microcavity index')

    freq_delta = 0.10
    ax.set_xlim(
        highQ_freq - freq_delta, 
        highQ_freq + freq_delta
        )
    ax.set_yticks([0, 0.5, 1])

    ax.set_xlabel(r'$f$')
    ax.set_ylabel(r'absolute fourier coefficients [a.u.]')

    fig.savefig(output)

if __name__ == '__main__':
    main()