'''
Reads DFT data of signals in cavities.
-> finds maxima
-> calculates quality factor from peaks
'''

import os
import numpy as np
import scipy.signal as signal
import sys
import pandas as pd

import matplotlib.pyplot as plt

import plotparams

def main():
    print('ANALYZING DFT')


    input = sys.argv[1]
    output = sys.argv[2]


    print('- reading csv')
    # get
    resonance_data = pd.read_csv(
        'singleres/resonances.csv',
        sep=', ',
        engine='python'
        )

    highQ_index = resonance_data['Q'].idxmax()
    highQ_mode = resonance_data.loc[highQ_index]
    highQ_freq = highQ_mode['frequency']
    highQ_Q = highQ_mode['Q']
    print('selected mode:')
    print('\tfreq:\t{0}'.format(highQ_freq))
    print('\tQ:\t{0}'.format(highQ_Q))

    f_min = highQ_freq - 0.05
    f_max = highQ_freq + 0.05
    print('\n-> using frequencies {0:.2f} - {1:.2f}'.format(f_min, f_max))


    print('- reading data...')

    data = np.loadtxt(input)
    frequency_list = data[0]
    uselist = data[1] # use leftmost resonator

    frequency_bool = (f_min<frequency_list) & (frequency_list<f_max)

    x_list = frequency_list[frequency_bool]
    y_list = uselist[frequency_bool]

    y_max = np.max(y_list) # for plotting later
    y_textdist = 0.25
    y_list = y_list / y_max

    # looking for peaks

    print('- analyzing...')

    peak_indices, peak_dict = signal.find_peaks(
        y_list,
        height=0.05
        )
    
    peak_widths = signal.peak_widths(y_list,peak_indices)
    

    # plotting
    print('- plotting...')

    fig, ax = plt.subplots()


    # plotting data
    ax.plot(x_list, y_list, color='black', zorder=3)

    # plotting peaks and widths
    for index, peak_data in zip(peak_indices, np.transpose(peak_widths)):
        # peak
        x = x_list[index]
        y = y_list[index]

        ax.axvline(x, color='red', zorder=2, linestyle='dashed')

        # full width half maximum (fwhm)
        fwhm_width = peak_data[0]
        fwhm_height = peak_data[1]
        fwhm_floatindices = peak_data[2:]

        fwhm_x_list = []
        for fi in fwhm_floatindices:
            # calculating positions of fwhm borders
            # from indizes given by peak_widths method
            # using interpolation

            fi_1 = int(str(fi).split('.')[0])
            fi_2 = float('0.' + str(fi).split('.')[1])

            x_1 = x_list[fi_1]
            x_2 = x_list[fi_1+1]

            x_fi = x_1 + (x_2-x_1) * fi_2

            fwhm_x_list.append(x_fi)
        
        ax.plot(
            fwhm_x_list, 
            [fwhm_height,fwhm_height], 
            color='red', 
            zorder=2,
            )
        

        # noting quality factor
        Q = x / (fwhm_x_list[1] - fwhm_x_list[0])

        if y == 1.:
            dy = -y_textdist
        else:
            dy = y_textdist
        
        ax.text(
            x, y + dy, r'$Q = {0:.0f}$'.format(Q),
            va='center', ha='center',
            rotation=90.,
            bbox=dict(
                facecolor='white', 
                edgecolor='red',
                boxstyle='round'
                )
            )

        
    ax.axvline(
        highQ_freq,
        color='lightgray',
        linestyle='-',
        linewidth=4.,
        zorder=1
        )

    ax.set_xlim(x_list[0], x_list[-1])

    ax.set_yticks([0, 0.5, 1.0])

    ax.set_xlabel(r'frequency')
    ax.set_ylabel(r'absolute fourier coefficient [a.u.]')

    fig.savefig(output)
    plt.close(fig)

if __name__ == '__main__':
    main()