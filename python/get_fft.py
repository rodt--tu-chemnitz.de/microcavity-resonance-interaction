import numpy as np
import pandas as pd
import json
import sys

import plotparams
import matplotlib.pyplot as plt

def main():
    print('PERFORMING FFT')
    
    # params

    input = sys.argv[1]
    output = sys.argv[2]


    ####################################################
    # read
    ####################################################

    print('- reading json')

    # load json
    with open('parameters.json') as f:
        params = json.load(f)

    SAMPLINGRATE = params['SAMPLINGRATE']


    print('- reading data')

    data = np.loadtxt(input)




    ####################################################
    # fourier transform of data
    ####################################################

    print('- performing fourier transform')

    fft_listlist = []

    for ez_list in data:
        fft_ez = np.fft.fftshift( np.fft.fft(ez_list) )
        fft_ez_abs = np.abs(fft_ez)


        fft_listlist.append(fft_ez_abs)
    
    fft_freqs = np.fft.fftshift( np.fft.fftfreq(len(fft_ez), d=SAMPLINGRATE) )

    print('- saving')

    np.savetxt(
        output, 
        [fft_freqs, *fft_listlist]
        )


if __name__ == '__main__':
    main()