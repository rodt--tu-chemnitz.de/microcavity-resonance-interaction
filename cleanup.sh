args="-v -f -r"

rm $args *.csv
rm $args *.log
rm $args *.h5
rm $args *.gif
rm $args *.png

rm $args csv_multires data h5 images TEMP results logs
