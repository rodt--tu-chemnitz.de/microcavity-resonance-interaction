set -e
###########################################
# This script exists to use scripts on stashed data
# which are designed for unstashed data
###########################################
#
# so:   1. unstashing data
#       2. applying script
#       3. stashing data again


NRES=2
df=0.1

mv results/df=0.1_NRES=${NRES}/* .
mkdir -p images/resonances
python python/plot_resonance_properties.py ${NRES}
python python/analyze_fft.py ${NRES}
python python/plot_resonancesfromfft.py
sh stash_data.sh ${df} ${NRES}