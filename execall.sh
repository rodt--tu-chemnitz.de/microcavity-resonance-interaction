set -e

mkdir -p logs

# one resonator
sh exec_singleres.sh
# mkdir -p images/snapshots
# python python/snapshots_calcres.py

df=0.1
NRES=$1

# multiple resonators
echo "---------- vary_dist.sh ---------"
echo
sh vary_dist.sh $df $NRES

# plot snapshots of fields
# echo "python/snapshots_coupling.py"
# mkdir -p images/snapshots
# python python/snapshots_coupling.py

echo
echo "---------- ANALYZING RESULTS ----------"
echo

echo "python/get_ez.py"
mkdir -p data/field
python python/get_ez.py $NRES
echo

echo "python/plot_ez.py"
mkdir -p images/field
python python/plot_ez.py
echo

echo "python/get_fft.py"
mkdir -p data/fft
python python/get_fft.py
echo

echo "python/plot_fft.py"
mkdir -p images/fft
python python/plot_fft.py
echo

echo "python/plot_resonance_properties.py"
mkdir -p images/resonances
python python/plot_resonance_properties.py $NRES
echo

echo "python/analyze_fft.py"
python python/analyze_fft.py ${NRES}
echo

echo "python/plot_resonancesfromfft.py"
python python/plot_resonancesfromfft.py
echo

sh stash_data.sh $df $NRES


python python/plot_harminv_results.py