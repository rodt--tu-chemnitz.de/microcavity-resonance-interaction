#################################################
# CALCULATING RESONANT MODES OF MULTIPLE RESONATORS
#################################################

set -e

# create necessary folders
mkdir -p h5
mkdir -p images/eps
mkdir -p images/ez
mkdir -p images/farfield
mkdir -p images/fields
mkdir -p images/fft
# mkdir -p TEMP
mkdir -p logs
mkdir -p csv_multires

mkdir -p data/fields
mkdir -p data/fft
mkdir -p data/farfield
mkdir -p data/ez

echo "CALCULATING RESONANT MODES OF MULTIPLE RESONATORS"
echo 

DIST=$1
echo "DIST = $DIST"
df=$2
echo "df = $df"
NRES=$3
echo "NRES = $NRES"

echo

diststr="DIST=$DIST"


######################################################
# calculating symmetric resonances
######################################################

# resonances
scriptname="multiple_resonators_resonances"
mode="resonances_symmetric"

echo "- calculating symmetric resonances using meep..."
python python/$scriptname.py $DIST $df $NRES symmetric > logs/$scriptname-${diststr}-$mode.log
# grep harminv results
cat logs/$scriptname-${diststr}-$mode.log | grep harminv0 | cut -c 12- > csv_multires/resonances-${diststr}-$mode.csv

# ez-fields
echo "    -> plotting farfield"
python python/plot_farfield.py data/farfield/farfield_${mode}_${diststr}.dat images/farfield/farfield_${mode}_${diststr}.png > /dev/null
echo "    -> plotting fields"
python python/plot_fields.py data/fields/fields_${mode}_${diststr}.dat images/fields/fields_${mode}_${diststr}.png > /dev/null

# fft
echo "    -> performing FFT"
python python/get_fft.py data/fields/fields_${mode}_${diststr}.dat data/fft/fft_${mode}_${diststr}.dat > /dev/null
echo "    -> plotting FFT"
python python/plot_fft.py data/fft/fft_${mode}_${diststr}.dat images/fft/fft_${mode}_${diststr}.png > /dev/null
echo "    -> searching spectrum for resonances"
python python/analyze_fft.py data/fft/fft_${mode}_${diststr}.dat images/fft/analysis_${mode}_${diststr}.png > /dev/null

mv $scriptname-eps-000000.00.h5 h5/eps-${mode}-${diststr}.h5



# farfield
scriptname="multiple_resonators_farfield"
mode="farfield_symmetric"


echo "- calculating symmetric farfield using meep..."
python python/$scriptname.py $DIST $df $NRES symmetric > logs/$scriptname-${diststr}-$mode.log
# grep harminv results
# cat logs/$scriptname-${diststr}-$mode.log | grep harminv0 | cut -c 12- > csv_multires/resonances-${diststr}-$mode.csv

# ez-fields
echo "    -> plotting farfield"
python python/plot_farfield.py data/farfield/farfield_${mode}_${diststr}.dat images/farfield/farfield_${mode}_${diststr}.png > /dev/null
echo "    -> plotting fields"
python python/plot_fields.py data/fields/fields_${mode}_${diststr}.dat images/fields/fields_${mode}_${diststr}.png > /dev/null

# fft
echo "    -> performing FFT"
python python/get_fft.py data/fields/fields_${mode}_${diststr}.dat data/fft/fft_${mode}_${diststr}.dat > /dev/null
echo "    -> plotting FFT"
python python/plot_fft.py data/fft/fft_${mode}_${diststr}.dat images/fft/fft_${mode}_${diststr}.png > /dev/null
echo "    -> searching spectrum for resonances"
python python/analyze_fft.py data/fft/fft_${mode}_${diststr}.dat images/fft/analysis_${mode}_${diststr}.png > /dev/null

mv $scriptname-eps-000000.00.h5 h5/eps-${mode}-${diststr}.h5

exit

# ######################################################
# # calculating antisymmetric resonances
# ######################################################

# # resonances
# scriptname="multiple_resonators_resonances"
# mode="resonances_antisymmetric"

# echo "- calculating antisymmetric resonances using meep..."
# python python/$scriptname.py $DIST $df $NRES antisymmetric > logs/$scriptname-${diststr}-$mode.log
# # grep harminv results
# cat logs/$scriptname-${diststr}-$mode.log | grep harminv0 | cut -c 12- > csv_multires/resonances-${diststr}-$mode.csv

# # ez-fields
# echo "    -> plotting farfield"
# python python/plot_farfield.py data/farfield/farfield_${mode}_${diststr}.dat images/farfield/farfield_${mode}_${diststr}.png > /dev/null
# echo "    -> plotting fields"
# python python/plot_fields.py data/fields/fields_${mode}_${diststr}.dat images/fields/fields_${mode}_${diststr}.png > /dev/null

# # fft
# echo "    -> performing FFT"
# python python/get_fft.py data/fields/fields_${mode}_${diststr}.dat data/fft/fft_${mode}_${diststr}.dat > /dev/null
# echo "    -> plotting FFT"
# python python/plot_fft.py data/fft/fft_${mode}_${diststr}.dat images/fft/fft_${mode}_${diststr}.png > /dev/null
# echo "    -> searching spectrum for resonances"
# python python/analyze_fft.py data/fft/fft_${mode}_${diststr}.dat images/fft/analysis_${mode}_${diststr}.png > /dev/null

# mv $scriptname-eps-000000.00.h5 h5/eps-${mode}-${diststr}.h5



# # farfield
# scriptname="multiple_resonators_farfield"
# mode="farfield_antisymmetric"


# echo "- calculating antisymmetric farfield using meep..."
# python python/$scriptname.py $DIST $df $NRES antisymmetric > logs/$scriptname-${diststr}-$mode.log
# # grep harminv results
# # cat logs/$scriptname-${diststr}-$mode.log | grep harminv0 | cut -c 12- > csv_multires/resonances-${diststr}-$mode.csv

# # ez-fields
# echo "    -> plotting farfield"
# python python/plot_farfield.py data/farfield/farfield_${mode}_${diststr}.dat images/farfield/farfield_${mode}_${diststr}.png > /dev/null
# echo "    -> plotting fields"
# python python/plot_fields.py data/fields/fields_${mode}_${diststr}.dat images/fields/fields_${mode}_${diststr}.png > /dev/null

# # fft
# echo "    -> performing FFT"
# python python/get_fft.py data/fields/fields_${mode}_${diststr}.dat data/fft/fft_${mode}_${diststr}.dat > /dev/null
# echo "    -> plotting FFT"
# python python/plot_fft.py data/fft/fft_${mode}_${diststr}.dat images/fft/fft_${mode}_${diststr}.png > /dev/null
# echo "    -> searching spectrum for resonances"
# python python/analyze_fft.py data/fft/fft_${mode}_${diststr}.dat images/fft/analysis_${mode}_${diststr}.png > /dev/null

# mv $scriptname-eps-000000.00.h5 h5/eps-${mode}-${diststr}.h5

######################################################
# calculating signal response
######################################################

mode="signal"

echo "- calculating signal response using meep..."
python python/$scriptname.py $DIST $df $NRES signal > logs/$scriptname-DIST=${DIST}_signal.log
# grep harminv results
cat logs/$scriptname-DIST=${DIST}_signal.log | grep harminv0 | cut -c 12- > csv_multires/resonances_DIST=${DIST}_signal.csv

echo "    -> plotting farfield"
python python/plot_farfield.py data/farfield/farfield_${mode}_${diststr}.dat images/farfield/farfield_${mode}_${diststr}.png > /dev/null
echo "    -> plotting fields"
python python/plot_fields.py data/fields/fields_${mode}_${diststr}.dat images/fields/fields_${mode}_${diststr}.png > /dev/null

echo "    -> performing FFT"
python python/get_fft.py data/fields/fields_${mode}_${diststr}.dat data/fft/fft_${mode}_${diststr}.dat > /dev/null
echo "    -> plotting FFT"
python python/plot_fft.py data/fft/fft_${mode}_${diststr}.dat images/fft/fft_${mode}_${diststr}.png > /dev/null
echo "    -> searching spectrum for resonances"
python python/analyze_fft.py data/fft/fft_${mode}_${diststr}.dat images/fft/analysis_${mode}_${diststr}.png > /dev/null

mv $scriptname-eps-000000000.h5 h5/eps-${mode}-${diststr}.h5