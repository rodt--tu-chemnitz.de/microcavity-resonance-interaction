set -e

df=$1
NRES=$2
resultdir="results/df=${df}_NRES=${NRES}"

echo "Stashing data for df=$df, NRES=$2"

rm -rf $resultdir

mkdir -p $resultdir

mv h5 $resultdir
# rm -r h5

mv images $resultdir
mv logs $resultdir
mv data $resultdir
mv csv_multires $resultdir

echo "-> Done!"