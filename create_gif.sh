scriptname=$1

# create gif
echo "- creating gif..."
eps_file=$scriptname-eps-000000.00.h5
ez_file=$scriptname-ez.h5

echo "    -> converting h5 to images..."

mkdir -p TEMP
cd TEMP
# get dimension
maxindex=$(h5ls $scriptname-ez.h5  | grep -E --only-matching [[:digit:]]*/Inf | grep -E --only-matching [[:digit:]]*)
maxindex_used=$((maxindex - 1))
# convert to pngs
h5topng -t 0:$maxindex_used -R -Zc dkbluered -A $eps_file -a gray $ez_file
# convert pngs to gif
echo "    -> creating $scriptname.gif..."
convert $scriptname-ez.t*.png $scriptname.gif
# delete single images
echo "    -> deleting images..."
rm -r $scriptname-ez.t*.png

cd ..