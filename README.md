# Microcavity Resonance Interaction

Analyze the interaction of two resonating microcavities using the Wigner function. This program contains several moving parts, which are used mainly by executing the `shell` scripts in the master folder.

Most calculations are done using the open-source library `meep`, some use `numpy` for FFT.


## How to use

If you start from scratch, e.g. by cloning the repository, you need to calculate the resonance frequencies of a single microdisc resonator. For this, execute `exec_singleres.sh`. This will calculate the resonant frequencies of whispering-gallery modes in a single microdisc cavity. The results will be saved in a CSV file in `csv/` and a GIF animation of the resulting field in the cavity will be produced.

In the following you can calculate the resonant frequencies of two coupled microcavities by executing `exec_multires.sh DIST DF`, where you can specify the distance between the resonators with `DIST` and the frequency range of the source `DF`. Note that smaller `DF` lead to longer calculation times, as the source needs to be turned on and off more slowly in order to excite fewer frequencies. All results obtained this way will be saved in a folder structure, and GIFS can be created for each parameter set if necessary. This takes a long time however, as the resulting h5 files can be more than 1GB in size.

A whole parameter space concerning `DIST` and `DF` can be examined using `vary_dist.sh DF`. All results can be examined using various scripts, which can also be executed automatically using `execall.sh`. The exact structure of the latter might change in the future, as it is not really necessary to vary the source most of the time.

Additional aspects can be analyzed by taking a look at the `archive/`, where e.g. Husimi functions on the disc surface are calculated.


## Scripts

In the following the purpose of each single script will be given in alphabetical order, as to give an overview over the moving parts of the program.

### Bash

Let's begin with the `bash` scripts in the master folder:
* `cleanup.sh` - Removes all files in upper hierarchy. Does not affect saved data using `stash_data.sh`
* `create_gif.sh` - Creates a GIF animation in the `TEMP/` folder using h5 files for fields and epsilon information. `h5topng` creates PNG from the data, which are stitched together to create a moving image. In the end, all PNG are deleted.
* `exec_multires.sh` - Calculates the field in two resonators and saves resonant frequencies, field and epsilon data of the system. `exec_singleres.sh` needs to be executed beforehand (at least once), so the source knows which frequency to excite.
* `exec_singleres.sh` - Calculates the field in a single resonator and saves resonant frequencies, field and epsilon data of the system.
* `execall.sh` - Executes `vary_dist.sh` and analyzes the resulting data using the python scripts. In the end stashes the data
* `stash_data.sh` - Saves data to subfolder which notes the number of oscillators and frequency width, so other systemparameters can be calculated without loss of data.
* `vary_dist.sh` - Calculates `exec_multires.sh` for various inter-cavity distances, in order to explore its influence.

### Python

Python scripts in alphabetic order:
* `__init__.py` - Empty script, so the other scripts in the folder can be used as a module.
* `calculate_resonances.py` - Calculates the resonance frequencies inside a single cavity. Use with `exec_singleres.sh` to have everything stored and evaluated automatically.
* `get_ez.py` - Reads $E_z$ at a certain point to analyze it via fourier transform.
* `get_fft.py` - Calculates FFT using the fields extracted with `get_ez.py`
* `multiple_resonators_calcmode.py` - Calculates mode structure for eigenmode of multiple resonators. Not working consistently due to convergence issues.
* `multiple_resonators.py` - Calculates Field in multiple resonators, the number of which can be input via command line using `argv`. The same can be done to input the intercavity distance. Use with `exec_multires.sh` for optimal results.
* `plot_ez.py` - Plots fields, which were extracted by `get_ez.py`.
* `plot_fft_compareresults.py` - TBD
* `plot_fft.py` - Plots fields, which were extracted by `get_ez.py`.
* `plot_harminv_results.py` - TBD
* `plotplarams.py` - This file is to be read only and sets plot parameters to make them consistent and beautiful (in my opinion).
* `snapshots_calcres.py` - TBD
* `snapshots_coupling.py` - TBD


### Archive

TBD